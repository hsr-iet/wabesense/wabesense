//
// NopSCADlib Copyright Chris Palmer 2020
// nop.head@gmail.com
// hydraraptor.blogspot.com
//
// This file is part of NopSCADlib.
//
// NopSCADlib is free software: you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// NopSCADlib is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with NopSCADlib.
// If not, see <https://www.gnu.org/licenses/>.
//
include <NopSCADlib/core.scad>
include <NopSCADlib/vitamins/pcbs.scad>
include <NopSCADlib/vitamins/smds.scad>
include <NopSCADlib/printed/butt_box.scad>
include <NopSCADlib/vitamins/sheets.scad>
include <NopSCADlib/vitamins/pcb.scad>

use <NopSCADlib/vitamins/pcb.scad>
use <NopSCADlib/vitamins/insert.scad>

$explode = 0;
box = bbox(screw = M3_dome_screw, sheets = DiBond, base_sheet = MDF6, top_sheet = glass2, span = 350, size = [90, 63, 12]);

module bbox_assembly() _bbox_assembly(box);

module bbox_test() {
    translate_z(bbox_height(box) / 2)
        bbox_assembly();
}

gt_5x17 = ["gt_5x17",    5,   10,   17, 5,   11, 0.4,  9,   2,1.5,     1,  3,   6,    0.5, 0,  0,   0];

test_pcb = ["TestPCB", "Test PCB",
    60, 60, 1.6, // length, width, thickness
    3,      // Corner radius
    2.75,   // Mounting hole diameter
    5.5,    // Pad around mounting hole
    "gray",// Color
    true,   // True if the parts should be separate BOM items
    // hole offsets
    [ [3, 3], [3, -3], [-3, 3], [-3, -3] ],
    // components
    [
        [ 22,  30, 180, "gterm", gt_5x17, 8, [2,5],"DarkSlateBlue"],
        [ 38,  30, 0, "gterm", gt_5x17, 8, [2,5], "DarkSlateBlue"],
        [ 30, 5,   0, "button_6mm"],
        [ 25,  6,   0, "smd_led", LED0805, "red"],
        [ 25,  4,   0, "smd_led", LED0805, "yellow"],
        [ 25,  2,   0, "smd_led", LED0805, "green"],

        [ 30,  15,   0, "smd_led", LED0805, "green"],
        [ 30,  30,   0, "smd_led", LED0805, "green"],
        [ 30,  45,   0, "smd_led", LED0805, "green"],

    ],
    // accessories
    []
];


if($preview)
    let($show_threads = true)
       translate([0,0,12]) pcb(test_pcb);
       translate([0,0,0]) rotate([0,0,90]) pcb(ArduinoLeonardo);
       difference (){
            bbox_test();
            translate([-15,-40,1.8]) cube([7,10,2.5]);
            translate([15,-40,2]) cube([8,20,10]);
       }
       color("RoyalBlue") translate([-19,-37,8]) cube([15,20,1.5]);