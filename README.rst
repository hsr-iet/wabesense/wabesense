=========
WABEsense
=========

The sensorization of existing alpine spring capture boxes and the generation of year-round discharge charts allow sustainable water management. Custom build boxes will be sensorized and their discharge curve determined using CFD

Installation
------------

For development
***************

To install all dependencies for development use the ``requirements.txt`` file::

    python -m pip install -U requirements.txt

Documentation
-------------

The user manual is hosted at https://hsr-iet.gitlab.io/wabesense/wabesense/

To build the documentation you need `Sphinx <https://www.sphinx-doc.org/en/master/>`_.
Make sure you have all the dependencies, see section `For development`_ in the
installation instructions.
Once you have all the dependencies, go to the folder ``doc`` and run::

    make html

The documentation is then available inside ``doc/build/html``. To read it, open
the ``index.html`` file in that folder with your favorite internet browser.