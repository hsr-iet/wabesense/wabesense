"""
    Analyze manual measurements
"""

# Copyright (C) 2021 OST Ostschweizer Fachhochschule
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

import sys
import argparse
from pathlib import Path
import pandas as pd

def parse_arguements():
    parser = argparse.ArgumentParser()
    parser.description = 'Analyze manual measurements. '

    parser.add_argument('file', type=str,
                        help='Full path to .xlsx file with manual measurements.')

    args = parser.parse_args(sys.argv[1:])
    args.file = Path(args.file).resolve()
    if not args.file.exists():
        raise ValueError(f'File {args.file} not found!')

    return args


if __name__ == '__main__':
    args = parse_arguements()
    df = pd.read_excel(args.file).dropna()

    # format datetime
    dt = pd.DatetimeIndex(pd.to_datetime(df['datetime[Europe/Zurich]']))
    # Excel does not support time zones
    #df['datetime[Europe/Zurich]'] = dt.tz_localize('Europe/Zurich')
    #df.rename(columns={'datetime[Europe/Zurich]':'datetime'}, inplace=True)
    df['datetime[Europe/Zurich]'] = dt

    # get date
    df['date'] = df['datetime[Europe/Zurich]'].dt.date.astype(str)

    # columns to group by
    cols = ['location', 'building', 'spring', 'date', 'volume_id']
    # FIXME this joins all campaigns. At the moment there is only one, so it is
    # OK, but when more reference measurements are available fix this to split by
    # a reasonable interval, e.g. group also by day.
    def ratio(x): return (x.std() / x.mean()).round(2)
    res = df.groupby(cols).agg(['mean', 'std', ratio])
    for k, g in df.groupby(cols).groups.items():
        res.loc[k, 'start [CET]'] = df['datetime[Europe/Zurich]'][g].min()
        res.loc[k, 'end [CET]'] = df['datetime[Europe/Zurich]'][g].max()

    outfile = args.file.with_name(args.file.stem + '_summary' + args.file.suffix)
    res.drop(columns=['volume [L]', 'wabe_type'], inplace=True)
    res.round(2).to_excel(outfile)

    print(res)
