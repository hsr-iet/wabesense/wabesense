"""
Plot data from WABEsense datalogger
"""

# Copyright (C) 2022 OST Ostschweizer Fachhochschule
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

import sys
from pathlib import Path
import datetime as dt

import pandas as pd
from matplotlib.backends.backend_pdf import PdfPages
import matplotlib.pyplot as plt


#: Path to data
DATAPATH = Path('../data').resolve()

#: Data CSV format
CSVFMT = {'sep':';'}

#: Logger sensors cols
logger_cols = ['BME_Pressure(Pa)',
               'BME_Temperature(C)',
               'BME_Humidity(%)',
               'VBat(V)']


def multipage_pdf(filename, figs=None, dpi=600):
    pp = PdfPages(filename)
    if figs is None:
        figs = [plt.figure(n) for n in plt.get_fignums()]
    for fig in figs:
        fig.savefig(pp, format='pdf', dpi=dpi)
    pp.close()


def parsefile(fname):
    """ Read data file """
    data = pd.read_csv(fname, **CSVFMT)
    data['datetime'] = pd.to_datetime(data['RTC_Date(yyyy.mm.dd)'] + ' ' 
                                      + data['RTC_Time(hh:mm:ss)'], utc=True)
    data['AutoMeas'] = data['AutoMeas'].astype(bool).astype('category')
    data.set_index('datetime', inplace=True)

    sensor_col = [x for x in data.columns
                  if not x.startswith('RTC_') and x not in logger_cols and x != 'AutoMeas']

    def V_to_mbar(x):
        return x * 100.0 / 5.0

    def V_to_C(x):
        return x * (50 + 40) / 5.0 - 40

    applymap = {}
    for c in sensor_col:
        if 'press' in c.lower():
            applymap[c] = V_to_mbar
        elif 'temp' in c.lower():
            applymap[c] = V_to_C
        else:
            raise ValueError(f'Unrecognized sensors column {c}')
    data[sensor_col] = data.transform(applymap)

    namesmap = {}
    for c in sensor_col:
        if 'press' in c.lower():
            namesmap[c] = c.replace('(V)', ' [mbar]')
        elif 'temp' in c.lower():
            namesmap[c] = c.replace('(V)', ' [C]')
        else:
            raise ValueError(f'Unrecognized sensors column {c}')
    data.rename(columns=namesmap, inplace=True)

    if any(x in fname.name.lower() for x in ('zug', 'hergiswil')):
        data = data[data.index >= dt.datetime(year=2021, month=1, day=13,
                                              hour=14, minute=30).astimezone()]

    return data


def parseref(file):
    """ Read reference manual measurements file """
    ref = pd.read_csv(file, sep=';', skiprows=6,
                             usecols=['Datetime', 'Q1 [L/min]', 'Q2 [L/min]'],
                             parse_dates=True,
                             index_col='Datetime')
    q1 = ref['Q1 [L/min]'].rename({'Q1 [L/min]': 'Q [L/min]'})
    q2 = ref['Q2 [L/min]'].rename({'Q2 [L/min]': 'Q [L/min]'})
    ref = pd.DataFrame(q1.append(q2), columns=['Q [L/min]'])
    return ref.dropna()


def plot_loggersensors(data):
    """ plot logger sensors """
    df = data[logger_cols][data.AutoMeas]
    fig, axs = plt.subplots(nrows=len(logger_cols), sharex='all')


if __name__ == '__main__':

    file = Path(sys.argv[1]).resolve()
    data = parsefile(file)
    to_drop = [x for x in data.columns if x.startswith('RTC_')] + ['AutoMeas']

    # Sensor
    df = data[data.AutoMeas].drop(columns=to_drop + logger_cols)
    fig, axs = plt.subplots(nrows=df.columns.size, sharex='all')
    fig.tight_layout()
    #mng = plt.get_current_fig_manager()
    #mng.resize(*mng.window.maxsize())
    fig.set_size_inches(11-3/4, 8-1/4, forward=True)

    for i, axlbl in enumerate(zip(axs, df.columns)):
        ax, lbl = axlbl
        df[lbl].plot(ax=ax, xlabel=None, color=f'C{i}')
        ax.set_ylabel(lbl)
        ax.set_xlabel('')
        #ax.autoscale(enable=True, axis='x', tight=True)
    plt.subplots_adjust(bottom=0.075)

    plt.savefig(file.with_name(file.stem + '_sensors.png'), dpi=fig.dpi,
                bbox_inches='tight', pad_inches=0.1)
    fig.suptitle(file.name + ': sensors')

    # Logger
    df = data[data.AutoMeas].drop(columns=to_drop + df.columns.to_list())
    fig, axs = plt.subplots(nrows=df.columns.size, sharex='all')
    fig.tight_layout()
    #mng = plt.get_current_fig_manager()
    #mng.resize(*mng.window.maxsize())
    fig.set_size_inches(11-3/4, 8-1/4, forward=True)

    for i, axlbl in enumerate(zip(axs, df.columns)):
        ax, lbl = axlbl
        df[lbl].plot(ax=ax, xlabel=None, color=f'C{i}')
        ax.set_ylabel(lbl)
        ax.set_xlabel('')
        #ax.autoscale(enable=True, axis='x', tight=True)
    plt.subplots_adjust(bottom=0.075)

    plt.savefig(file.with_name(file.stem + '_logger.png'), dpi=fig.dpi,
                bbox_inches='tight', pad_inches=0.1)
    fig.suptitle(file.name + ': logger')

    #multipage_pdf(file.with_suffix('.pdf'), dpi=fig.dpi)

    plt.show()

