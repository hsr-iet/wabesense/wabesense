"""
Plot data from WABEsense datalogger
"""

# Copyright (C) 2022 OST Ostschweizer Fachhochschule
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

import sys
from pathlib import Path
import datetime as dt

import pandas as pd
import matplotlib.pyplot as plt
import matplotlib

import scipy.stats as scs
import numpy as np

matplotlib.rcParams['figure.figsize'] = (15, 10)

#: Path to data
DATAPATH = Path('../data').resolve()

#: Data CSV format
CSVFMT = {'sep':';'}

#: Logger sensors cols
zero_logger_cols = ['BME_Humidity(%%)',
                    'BME_Temperature(C)',
                    'BME_Pressure(Pa)',
                    'Battery_Voltage(V)']

date_col = 'RTC_Datetime(YYYY.mm.dd HH:MM:SS.MS+0000)'
auto_sample_col = 'Measurement_Auto'

#: Not Sensor columns
not_sensor_cols = zero_logger_cols + [date_col, auto_sample_col]

def voltage_to_physical(data, sensor_col):
    # Convert sensor voltages to physical units
    def V_to_mbar(x):
        return x * 100.0 / 5.0

    def V_to_C(x):
        return x * (50 + 40) / 5.0 - 40

    applymap = {}
    for c in sensor_col:
        if 'press' in c.lower():
            applymap[c] = V_to_mbar
        elif 'temp' in c.lower():
            applymap[c] = V_to_C
        else:
            raise ValueError(f'Unrecognized sensors column {c}')
    data[sensor_col] = data.transform(applymap)

    return data

def parsefile(fname):
    """ Read data file """
    # Read whole file
    data = pd.read_csv(fname, **CSVFMT)
    # Drop NA columns
    data.drop(columns=data.columns[data.columns.str.startswith("NA")], inplace=True)
    # set datetime tz info to UTC
    data['datetime'] = pd.to_datetime(data[date_col], utc=True)
    # set datetime as index
    data.set_index('datetime', inplace=True)
    # Drop raw date column
    data.drop(columns=date_col, inplace=True)

    # Convert auto sample to boolean category
    data[auto_sample_col] = data[auto_sample_col].astype(bool).astype('category')
    
    # Convert sensor voltages to physical units
    sensor_col = [x for x in data.columns if x not in not_sensor_cols]
    voltage_to_physical(data, sensor_col)
    
    # Rename sensor columns
    namesmap = {}
    for c in sensor_col:
        if 'press' in c.lower():
            namesmap[c] = c.replace('(V)', ' [mbar]')
        elif 'temp' in c.lower():
            namesmap[c] = c.replace('(V)', ' [C]')
        else:
            raise ValueError(f'Unrecognized sensors column {c}')
    data.rename(columns=namesmap, inplace=True)

    # renaming
    logger_cols = list(map(lambda x: x.split('(')[0].lower(), zero_logger_cols))
    data.rename(columns={o:n for o,n in zip(zero_logger_cols, logger_cols)},
                inplace=True)
    return data, logger_cols

def set_axes_freq(axs, freq, *, fmt=None):
    try:
        axs[0]
    except TypeError:
        axs = [axs]

    if fmt is None:
        fmt = "%H:%M\n%d/%m"

    x = axs[0].get_lines()[0].get_xdata()
    try:
        t = pd.date_range(x[0], x[-1], freq=freq)
    except ValueError:  # expecting Timestamp
        t = pd.date_range(x[0].to_timestamp(), x[-1].to_timestamp(), freq=freq)
        
    tl = [u.strftime(fmt) for u in t]
    [(ax.set_xticks(t), ax.set_xticklabels(tl)) for ax in axs]


if __name__ == '__main__':

    df, log_cols = parsefile('zero-series-test.csv')
    z_cols = ['zero: ' + x for x in log_cols] 
    df_a, log_cols = parsefile('alpha-series-test.csv')
    a_cols = ['alpha: ' + x for x in log_cols]
    
    # Logger
    tmp_ = df.rename(columns={o:n for o,n in zip(log_cols, z_cols)})
    axs = tmp_[z_cols[:-1]].plot(subplots=True)
    tmp_ = df_a.rename(columns={o:n for o,n in zip(log_cols, a_cols)})
    axs = tmp_[a_cols[:-1]].plot(ax=axs, subplots=True, grid=True, style='--')
    df['temperature [C]'].plot(label='zero: sensor T', ax=axs[1], legend=True, grid=True)
    
    mng = plt.get_current_fig_manager()
    mng.resize(*mng.window.maxsize())
    
    # Overview plot
    axis_freq = '1w'
    plt.gcf().tight_layout()
    set_axes_freq(axs, axis_freq)
    plt.xlabel('datetime UTC')

    # Fit humidity
    fig, ax = plt.subplots()
    idx0, idx1 = df.index[df.bme_humidity < 30][[0, -1]]
    x = (df.loc[idx0:idx1].index - idx0).total_seconds() / 60 / 1440
    fit = scs.linregress(x, df.bme_humidity[idx0:idx1].to_numpy())
    linf = f'{fit.slope:.2f} [%/day] t + {fit.intercept:.1f} [%]'
    df.loc[idx0:idx1, linf] = fit.slope * x + fit.intercept

    ax = df.loc[idx0:idx1, ['bme_humidity', linf]].plot(ax=ax, grid=True)
    mng = plt.get_current_fig_manager()
    mng.resize(*mng.window.maxsize())
    plt.gcf().tight_layout()

    set_axes_freq(ax, axis_freq)
    plt.xlabel('datetime UTC')

    # Compare pressures
    fig, axs = plt.subplots(nrows=2)
    p = pd.concat((df['bme_pressure'].resample('10T').mean().rename('zero bme P'),
                   df_a['bme_pressure'].resample('10T').mean().rename('alpha bme P')),
                   axis=1) * 1e-2
    dp = p['zero bme P'] - p['alpha bme P']
    p.plot(ax=axs[0], ylabel='pressure [mbar]', grid=True)
    dp.plot(ax=axs[1], ylabel='pressure difference zero - alpha [mbar]', grid=True)
    set_axes_freq(axs, axis_freq)
    plt.xlabel('datetime UTC')
    axs[1].set_ylim(-2,2)
    mng = plt.get_current_fig_manager()
    mng.resize(*mng.window.maxsize())
    plt.gcf().tight_layout()

    plt.show()


