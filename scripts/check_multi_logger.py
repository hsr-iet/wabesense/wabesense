"""
This script is used to check the data recorded by several loggers.
It is used to check they work properly
"""
from typing import Union
from pathlib import Path
import logging
import sys
import warnings

import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import yaml

formatter = logging.Formatter(
        "%(asctime)s %(levelname)s:%(name)s: %(message)s",
        datefmt="%d.%m.%Y %H:%M:%S %Z",
    )

log_name = Path(__file__).stem
logger = logging.getLogger(log_name)
logger.setLevel(logging.DEBUG)

# stdout logging
consoleHandler = logging.StreamHandler(sys.stdout)
consoleHandler.setLevel(logging.INFO)


def std_filter(x):
    return ((x.name == log_name) or (logger.name in x.name)) and (
        x.levelno == logging.INFO
    )


consoleHandler.addFilter(std_filter)

# file logging
log_file = log_name + ".log"
fileHandler = logging.FileHandler(log_file, mode="w")
fileHandler.setFormatter(formatter)

if not len(logger.handlers):
    logger.addHandler(fileHandler)
    logger.addHandler(consoleHandler)


def read_info(fpath: Path) -> dict:
    """ Parse info file as dictionary."""
    logger.info(f"Reading info from {fpath}")
    with fpath.open("r") as file:
        #  BaseLoader does not resolve or support any tags and construct only
        #  basic Python objects: lists, dictionaries, and Unicode strings
        info = yaml.load(file, Loader=yaml.BaseLoader)
    logger.debug(info)
    return info


def data_fpath(dev_name: str, *, folder=None):
    if folder is None:
        folder = Path().cwd()
    return folder / Path(dev_name + ".csv")


def read_data(fpath: Union[str, Path]) -> pd.DataFrame:
    """ Read data file into pandas data frame.

        Parameters
        ----------
        fpath:
            Full path to CSV file with the data.

        Returns
        -------
        :class:`pandas.DataFrame`
    """
    logger.info(f'Reading {fpath} using pandas ...')
    data = pd.read_csv(fpath, sep=";")
    logger.debug(data)

    # Mask invalid Measurement_Auto as sign of malformed data
    # See issue #22 https://gitlab.com/hsr-iet/wabesense/wabesense_server/-/issues/22
    msk = (data["Measurement_Auto"] != 0) & (data["Measurement_Auto"] != 1)
    if msk.any():
        wrn_ = RuntimeWarning(f"Malformed entries in the raw data (count {msk.sum()})")
        warnings.warn(wrn_)
        logger.warning(wrn_)
        logger.debug(f"Last correct value {data.loc[~msk, :].iloc[-1]}")
        data = data.loc[~msk, :]

    data['datetime'] = pd.to_datetime(data['RTC_Datetime(YYYY.mm.dd HH:MM:SS.MS+0000)'],
                                      utc=True)
    data.drop(columns=['RTC_Datetime(YYYY.mm.dd HH:MM:SS.MS+0000)'], inplace=True)
    data.set_index('datetime', inplace=True)
    # drop NA columns
    msk = data.columns.str.startswith("NA")
    data.drop(columns=data.columns[msk], inplace=True)

    return data

def V_to_scale(V: Union[pd.Series, pd.DataFrame], *,
               min: float, max: float, scale: float = 5.0) -> Union[pd.Series, pd.DataFrame]:
    return V / scale * (max - min) + min


if __name__ == "__main__":
    folder = Path(sys.argv[1])  # 1st argument is path to folder with datA

    # get all info files
    logger.info(f"Getting info files from {folder} ...")
    info_files = folder.glob("*.txt")
    info_dicts = []
    for file in info_files:
        try:
            devinfo = read_info(file)
        except yaml.parser.ParserError:
            logger.debug(f"{file} doesn't seem to be a yaml file. Ignoring")
            continue
        if "Device name" in devinfo:
            info_dicts.append(devinfo.copy())
            logger.debug(f"{file} isn't a logger info file. Ignoring")
    logger.info(f"Found {len(info_dicts)} info files.")

    # get data
    if info_dicts:
        logger.info(f"Parsing data ...")
        data = []
        for dev_name in [str(d["Device name"]) for d in info_dicts]:
            fpath = data_fpath(dev_name, folder=folder)
            if fpath.exists():
                df = read_data(fpath)
                # extract sensor name and rename column
                pcol_ = df.columns[df.columns.str.endswith("sure(V)")][0]
                sensor_name = pcol_.split(".")[-2]
                df.rename(columns=
                          lambda x: x.split(".")[-1] if sensor_name in x else x,
                          inplace=True)
                df = pd.concat({(dev_name, sensor_name): df}, names=["device", "sensor"])
                data.append(df)
            else:
                logger.info(f"{fpath} not found")
        if data:
            data = pd.concat(data)
        else:
            logger.info("No data read. Done")
            sys.exit()
    else:
        logger.info("No data files to read. Done")

    # convert to physical
    sensor_cols = ["pressure(V)", "temperature(V)"]
    for col,m,M,u in zip(sensor_cols, [0.0, -40.0], [100.0, 50.0], ["mbar", "C"]):
      logger.info(f"Converting {col} to physical units ...")
      data[col] = V_to_scale(data[col], min=m, max=M)
      data.rename(columns={col: col.split("(")[0] + f" [{u}]"}, inplace=True)
    sensor_cols = ["pressure [mbar]", "temperature [C]"]

    plot_cols = ['Battery_Voltage(V)', 'BME_Temperature(C)', 'BME_Pressure(Pa)',
                 'BME_Humidity(%%)']
    fig, axs = plt.subplots(nrows=len(plot_cols),
                            constrained_layout=True, sharex="col")
    devs = []
    for dev, g in data.groupby("device"):
        logger.info(f"Adding {dev} signals to plots ...")
        devs.append(dev)
        g = g.reset_index(level=["device", "sensor"])
        for ax, c in zip(axs, plot_cols):
            g[c].plot(legend=False, ax=ax)
            ax.set_ylabel(c.split("(")[0])
    fig.legend(devs)
    
    plot_cols =  sensor_cols
    nrows_ = len(plot_cols)
    fig, axs = plt.subplots(nrows=nrows_,
                            constrained_layout=True, sharex="col")
    for ax, lbl in zip(axs, plot_cols):
      devs = []
      for dev, g in data[lbl].groupby(["device", "sensor"]):
        devs.append(".".join(dev))
        logger.info(f"Adding {dev} signals to plots ...")
        g = g.reset_index(level=["device", "sensor"]).dropna(how="all", axis=1)
        g.plot(ax=ax, legend=False)
        ax.set_ylabel(lbl)
    fig.legend(devs)
    plt.show()
