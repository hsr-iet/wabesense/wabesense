
"""
Plot data from STS sensor in M-WABE
"""

# Copyright (C) 2020 OST Ostschweizer FachHochschule
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

from pathlib import Path
import datetime as dt
import re
import warnings
import pdb

import pandas as pd
import matplotlib.pyplot as plt
import matplotlib
matplotlib.rcParams['font.size'] = 16
matplotlib.rcParams['figure.figsize'] = (15, 10)

import seaborn as sns
import numpy as np

import scipy.stats as sps
from scipy import odr

#: Path to data
DATAPATH = Path('../data').resolve()


#: Data CSV format
CSVFMT = {'encoding':'latin', 'sep':';'}

#: date parser
dateparser = lambda date: dt.datetime.strptime(date, '%d.%m.%Y %H:%M:%S')
dateparser_m = lambda date: dt.datetime.strptime(date, '%Y-%m-%d %H:%M:%S')

#: Reference measurements
Q_Loebbia = dict(
    datetime=list(map(dateparser,
                      ['29.10.2020 00:00:00',  # corrected to have logged value, original: '28.10.2020 10:30:00'
                       '09.11.2020 14:45:00'] +
                      ['23.11.2020 14:40:00', '04.06.2021 10:45:00'] +
                      ['15.06.2021 14:50:00', '28.06.2021 14:30:00'] +
                      ['12.07.2021 14:40:00', '26.07.2021 14:25:00'] +
                      ['15.12.2020 12:00:00', '20.01.2021 12:00:00'] +
                      ['17.02.2021 12:00:00', '17.03.2021 12:00:00'] +
                      ['15.04.2021 12:00:00', '26.05.2021 12:00:00'])),
    Q_Lmin=[2452.0, 2000.0, 1341.0, 1565.0, 1809.0, 1624.0, 1784.0, 2495.0] +
           [1200, 1100, 900, 900, 1000, 1500],
    spring=['Löbbia']*14,
    WABEId=['']*14,
    WABEType=[3]*14,
    loggerData=['Löbbia/Löbbia_logger.csv']*14)

St_Moritz = dict(
    datetime=list(map(dateparser, ['15.10.2020 8:35:00', '11.11.2020 11:30:00'] +
                              ['15.10.2020 10:30:00', '11.11.2020 14:00:00'] +
                              ['15.10.2020 9:10:00', '11.11.2020 12:35:00'])),
    Q_Lmin=[158.6, 125.0, 63.4, 80.0, 148.0, 115.0],
    spring=['St. Moritz']*6,
    WABEId=['A2']*2 + ['E9+10']*2 + ['B5+6']*2,
    WABEType=[2]*6,
    loggerData=['St._Moritz/St__Moritz__M_WABEsense_A2_logger.csv']*2 +
               ['St._Moritz/St__Moritz__M_WABEsense_E9_10_logger.csv']*2 +
               ['St._Moritz/St__Moritz__M_WABEsense_B5_6_logger.csv']*2)

#: Options to parse a CSV file
PARSEOPT = {'skiprows': 7,
            'parse_dates': {'Datetime': ['Date', 'Time']},
            'date_parser': dateparser,
            'usecols': [3,4,5,7],
            'names': ['Date', 'Time', 'Pressure [mbar]', 'Temperature [C]'],
            'index_col': 'Datetime'}

PARSEOPT_WMS = {#'parse_dates': {'Datetime': 'datetime'},
                'date_parser': dateparser_m,
                'usecols': [0,1,2],
                'names': ['Datetime', 'Pressure [mH2O]', 'Temperature [C]'],
                'header': 0,
                'index_col': 'Datetime'}

# M-WABe data
# type 2 from drawing ROMAG M-WABE-SENSE-0100-Z1, article 234378
# type 3 from drawing SIGMATECH WABE Sense Ø700 Ident.-Nr. 10100697 05.10.2020
# all measures are in m measured from the top cover of the wabe
mwabedata = pd.DataFrame(data=dict(
    nozzle_zoffset=[550e-3, 577e-3],
    outlet_zoffset=[276e-3, 310e-3],
    outlet_diameter_o=[168.3e-3, 219.1e-3]),
                         index=pd.RangeIndex(2, 4, name='WABEType'))
# distance from nozzle middle to outlet invert
mwabedata['hQ0'] = mwabedata.nozzle_zoffset \
                   - (mwabedata.outlet_zoffset + mwabedata.outlet_diameter_o / 2)
# distance from nozzle middle to outlet top
mwabedata['hFS'] = mwabedata.nozzle_zoffset \
                   - mwabedata.outlet_zoffset + mwabedata.outlet_diameter_o / 2

# maximum pressure (mbar) without outflow mbar
pQ0 = mwabedata.hQ0 * 1e2
# maximum pressure (mbar) with free surface inside outlet
pFS = mwabedata.hFS * 1e2
# w.r.t pQ0
dpFS = mwabedata.outlet_diameter_o * 1e2  # same as pFS - pQ0
# maximum pressure (mbar) with free surface inside WABE
pFS_max = mwabedata.nozzle_zoffset * 1e2
# w.r.t pQ0
dpFS_max = pFS_max - pQ0


def parsefile(fname, wabetype, *, logger: str=None):
    """ Read data file """
    if logger == "WMS":
      data = pd.read_csv(fname, **PARSEOPT_WMS)
      data['Pressure [mbar]'] = data['Pressure [mH2O]'] / 0.0102
    else:
      data = pd.read_csv(fname, **CSVFMT, **PARSEOPT)
      # Correct pressures by resetting tara to pQ0
      p_tara = get_ptara(fname)
      data['Pressure [mbar]'] = data['Pressure [mbar]'] + p_tara
      data['Pressure [mH2O]'] = data['Pressure [mbar]'] * 0.0102
      # ignore values below pQ0
      data.loc[data['Pressure [mbar]'] < 0, 'Pressure [mbar]'] = np.nan

    data.index = data.index.tz_localize('UTC')
    data.drop(index=data.index[data.index.year < 2020], inplace=True)

    return data


def get_ptara(filepath):
    """ Get pressure used for tara from file """
    reg = re.compile(r'REF_VALUE=(?P<p_tara>\d+[.]?\d+)')
    try:
      txt = filepath.read_text()
    except UnicodeDecodeError:
      txt = filepath.read_text(encoding='Latin')
    return float(reg.search(txt).group('p_tara'))


def get_reference_pressure(press, qref, *, criterion='mean', period='1h'):
    qref = qref.dropna()
    pname = press.name.split()
    pname[0] = '_'.join([pname[0], criterion])  #, period])
    pname = ' '.join(pname)

    p = pd.DataFrame(index=qref.index, dtype=float, columns=[pname, pname+'_std'])
    tdh = pd.to_timedelta(period) / 2
    for t, q in zip(qref.index, qref):
        p_vals = press.loc[t-tdh:t+tdh].copy()
        p.loc[t, pname] = p_vals.agg(criterion)
        p.loc[t, pname+'_std'] = p_vals.std()
    if not p[pname].notna().any():
        warnings.warn('All pressure values are NaN or below 0')
    return p


def turbulent_flow(dp, *, fD, dx, D, rho):
    """ Pressurized turbulent flow.

        Parameters
        ----------
        dp: array_like
            Gauge pressure
        fD: float
            Friction factor Darcy–Weisbach equation, e.g., from Moody diagram.
        dx: float
            Pipe length
        D: float
            Pipe diameter
        rho: float
            Water density
    """
    v = np.zeros_like(dp)
    msk = dp > 0
    v[msk] = np.sqrt(2 * dp[msk] / rho / (4 * fD * (dx / D) + 1))
    q = (v * np.pi * D**2 / 4)
    return q, v


def ev_model(x, *, f_low, f_high, p_threshold):
    q = []
    for f in (f_low, f_high):
        q.append(f(x) if callable(f) else np.polyval(f, x))
    q = np.where(x < p_threshold, q[0], q[1])
    # Do not return negative flows
    q[q < 0] = 0.0
    return q


linear0intercept = odr.Model(lambda beta, x: beta[0] * x,
                             lambda beta, x: x.reshape(1, -1),
                             lambda beta, x: beta[0] * np.ones(x.size),
                             estimate=lambda d: [np.nanmean(d.y / d.x)] if d.x.size > 0 else 0)


# turbulent flow function
# monkey patch the diameter
def q_high(x):
    q, v = turbulent_flow(x * 1e2,  # pressure in Pa
                          fD=0.0125,  # friction factor Darcy–Weisbach equation (High Re, structural steel)
                          rho=998,  # kg/m^3 water density
                          dx=100.0,#19.0,  # m
                          D=q_high.D)
    return q * 6e4  # L/min

# Plot for MWS data from löbbia
def plot_mws(df: pd.DataFrame):

    fig, axs = plt.subplots(nrows=3, sharex="col", constrained_layout=True)

    ax = axs[0]
    pQ0 = mwabedata.hQ0[3]
    q_high.D = mwabedata.outlet_diameter_o[3]
    df["Q [L/min]"] = q_high((df["Pressure [mH2O]"]-pQ0)/1e2 * 9806.65)
    df["Q [L/min]"].plot(ax=ax, color="C2", ylabel="Q [L/min]", label="")
    df["Q [L/min]"].rolling('3h').mean().plot(ax=ax, color="k", label="3h avg.")
    ref_m = pd.Series(
    index = list(map(dateparser_m, ['2022-07-14 15:18:00', '2022-06-24 10:00:00', '2022-06-02 15:35:00', '2022-05-19 15:35:00',
                                    '2021-08-23 15:00:00', '2021-09-02 16:00:00'])),
    data=[1060.85,727,669,703,5688,3723])
    ref_m.index = ref_m.index.tz_localize('UTC')
    ref_m.plot(ax=ax, style="ok")

    ax = axs[1]
    df["Pressure [mH2O]"].plot(ax=ax, color="C0", ylabel="Pressure [mH2O]", label="")
    df["Pressure [mH2O]"].rolling('3h').mean().plot(ax=ax, color="k", label="3h avg.")
    ax.axhline(pQ0, color="k", linestyle="--", label="pQ0")
    # maximum pressure (mbar) with free surface inside outlet
    ax.axhline(mwabedata.hFS[3], color="m", linestyle="--", label="pFS")
    # maximum pressure (mbar) with free surface inside WABE
    ax.axhline(mwabedata.nozzle_zoffset[3], color="r", linestyle="--", label="pFS_max")
    ax.legend()

    ax = axs[2]
    df["Temperature [C]"].plot(ax=ax, color="C1", ylabel="Temperature [C]", label="")
    
    return ax, ref_m

def plot_pressure(P: pd.Series, *, pQ0: float, pFS: float, pFull: float,
                  avg_window: str = None):
    fig, ax = plt.subplots(constrained_layout=True)
    P.plot(ax=ax, style='.', label="", legend=False, ylabel=P.name)
    ls = dict(linestyle="--", linewidth=2,)
    # maximum pressure without flow
    ax.axhline(pQ0, color="g", label="pQ0 (Q = 0)", **ls)
    # maximum pressure (mbar) with free surface inside outlet
    ax.axhline(pFS, color="m", label="pFS (Q > 1'000 L/min )", **ls)
    # maximum pressure (mbar) with free surface inside WABE
    ax.axhline(pFull, color="r", label="pFS_max", **ls)

    ax.legend()

    # shade operational region
    x_ = ax.get_lines()[0].get_xdata()[[0,-1]]
    ax.fill_between(x_, pQ0, pFS, color="g", alpha=0.2, zorder=-1)
    # shade out of scale
    ax.fill_between(x_, pFS, pFull, color="r", alpha=0.2, zorder=-1)

    return ax, fig




if __name__ == '__main__':
    if 'reference_Q' not in locals():
        reference_Q = []
        for d in (Q_Loebbia,):# St_Moritz):
            df = pd.DataFrame(data=d).rename(columns={'Q_Lmin': 'Q [L/min]'})
            reference_Q.append(df)
        reference_Q = pd.concat(reference_Q, ignore_index=True).set_index('datetime')
        reference_Q.index = reference_Q.index.tz_localize('Europe/Zurich').tz_convert('UTC')

    dp_col = "dP [mbar]"
    reference_Q['discharge_model_odr1'] = pd.NA
    for fname, ref in reference_Q.groupby('loggerData'):
        wabetyp = ref.WABEType.values[0]
        print(f'Loading {fname} ...')

        data = parsefile(DATAPATH / fname, wabetype=wabetyp)
        data["dP [mbar]"] = data["Pressure [mbar]"] - pQ0[wabetyp]
        qref = ref[['Q [L/min]']].copy()
        qref['Q_std'] = [200] * qref.shape[0]
        qname, qstd = qref.columns

        poly_max = np.array([-np.inf, 0])
        w_max = ''
        # iterate over aggregation window until you get a model with positive
        # slope between pressure and flow
        for w in ('3h', '6h', '12h', '24h', '2d', '5d'):
            crit = 'median'
            pref = get_reference_pressure(data["dP [mbar]"], qref[qname],
                                          period=w,
                                          criterion=crit)
            pname, pstd = pref.columns
            pq = pd.concat((pref, qref), axis=1).dropna()
            pq['P(pressurized)'] = 1 - sps.norm(loc=pq[pname], scale=pq[pstd]).cdf(dpFS[wabetyp])
            pq['pressurized'] = pq['P(pressurized)'] > 0.75
            # Calibrate below pressurized
            pq_ = pq.loc[~pq.pressurized]
            d_ = odr.RealData(*pq_[[pname, qname]].to_numpy().T,
                              sx=pq_[pstd].to_numpy(), sy=pq_[qstd].to_numpy())

            try:
                # no intercept
                odr_obj = odr.ODR(d_, linear0intercept)
                output = odr_obj.run()
                poly = np.concatenate((output.beta, [0.]))

            except (ValueError, TypeError) as e:
                print('Calibration data is wrong')
                poly = np.array([np.nan, np.nan])

            poly_max, w_max = (poly, w) if poly[0] > poly_max[0] else (poly_max, w_max)

            if all(~np.isnan(poly)) and (poly[0] > 0):
                break

        poly, w_ = (poly, w) if poly[0] > 0 else (poly_max, w_max)

        if (poly[0] < 0) and (d_.x.size > 0):
              poly = np.asarray([np.nanmean(d_.y / d_.x), 0])

        print(pq)
        print()
        reference_Q.loc[pref.index, [pname, pstd]] = pref
        reference_Q.loc[qref.index, qstd] = qref[qstd]
        reference_Q.loc[pq.index, 'pressurized'] = pq.pressurized
        reference_Q.loc[pq.index, f'{crit}_window'] = w_
        reference_Q.loc[pq.index, 'discharge_model_odr1'] = [poly.reshape(-1, 1)] * pq.shape[0]


        # Evaluate
        q_high.D = mwabedata.outlet_diameter_o[wabetyp]
        data['Q [L/min]'] = ev_model(data["dP [mbar]"], f_low=poly,
                                     f_high=q_high, p_threshold=dpFS[wabetyp])

        # Plot
        tmp = data[['Q [L/min]', "dP [mbar]"]].asfreq('10T')
        qdata, pdata = tmp.columns
        ax = tmp.plot(subplots=True, legend=True)

        tmp_ = tmp.rolling('1h').mean()
        tmp_[qdata].plot(color='k', ax=ax[0], label='')
        tmp_[pdata].plot(color='k', ax=ax[1], label='')

        qref[qname].plot(style='or', ax=ax[0], legend=True, label='measurement')
        pref[pname].plot(style='or', ax=ax[1])

        if 'joint_model' in ref:
            jm = ref.joint_model.iloc[0]
            try:
                q_joint = ev_model(tmp_[pdata], f_low=jm,
                                   f_high=q_high, p_threshold=dpFS[wabetyp])
                q_joint = pd.Series(data=q_joint, index=data.index,
                                    name='Q joint [L/min]')
                q_joint.plot(alpha=0.5, ax=ax[0], label=q_joint.name, legend=True)
            except TypeError:
                pass

        if (data[pdata] >= dpFS[wabetyp]).any():
            ax[1].axhline(dpFS[wabetyp], ls='--', c='m', label='pressurized outlet')
        if (data[pdata] >= dpFS_max[wabetyp]).any():
            ax[1].axhline(dpFS_max[wabetyp], ls='--', c='r', label='pressurized WABE')

        pmax = ax[1].get_ylim()[1]
        p0 = data[pdata].min()
        ax[1].set_ylim(p0, pmax)

        xmin = min(data.index.min(), qref.index.min())
        xmax = max(data.index.max(), qref.index.max())
        ax[0].set_xlim(xmin - pd.Timedelta('1d'), xmax + pd.Timedelta('1d'))
        
        mng = plt.get_current_fig_manager()
        mng.resize(*mng.window.maxsize())
        fig = plt.gcf()
        title_txt = f'{ref.spring[0]} {(" " + ref.WABEId[0]) if ref.WABEId[0] else ""}'
        fig.suptitle(title_txt)
        fig.tight_layout()
        plt.legend()
        plt.savefig(title_txt.replace(' ', '_') +'.png')

        if (data[pdata] >= dpFS[wabetyp]).any():
            fig, ax = plt.subplots()
            p = np.linspace(-5, dpFS_max[wabetyp], 100)
            q = ev_model(p, f_low=poly, f_high=q_high, p_threshold=dpFS[wabetyp])
            ax.plot(p, q, label='model')
            ax.errorbar(pq[pname], pq[qdata],
                        xerr=pq[pstd], yerr=pq[qstd],
                        fmt='none', color='grey', zorder=0)
            sc = ax.scatter(pq[pname], pq[qdata], c=pq.pressurized, label='data')
            # produce a legend with the unique colors from the scatter
            legend1 = ax.legend(*sc.legend_elements(), loc="lower right",
                                title="pressurized")
            ax.add_artist(legend1)
            ax.axvline(dpFS[wabetyp], ls='--', c='m', label='pressurized outlet')
            ax.set_xlabel('Pressure over pQ0 [gauge mbar]')
            ax.set_ylabel(qdata)
            title_txt = title_txt + ' model'
            plt.title(title_txt)
            plt.legend()
            fig.tight_layout()
            plt.savefig(title_txt.replace(' ', '_') + '.png')



    # Put all data together
    # Make sense only for same WABE type
    reference_Q['joint_model'] = pd.NA #[np.nan * np.zeros(2)] * reference_Q.shape[0]
    for wbtyp, ref in reference_Q.groupby('WABEType'):
        if (ref.spring.nunique() > 1) or (ref.WABEId.nunique() > 1):
            msk = ~(ref.pressurized.astype(bool))
            pq = ref.loc[msk, [pname, qname, pstd, qstd]].dropna()
            d_ = odr.RealData(*pq[[pname, qname]].to_numpy().T,
                              sx=pq[pstd].to_numpy(), sy=pq[qstd].to_numpy())
            poly = np.concatenate((odr.ODR(d_, linear0intercept).run().beta, [0.]))
            print(f'Aggregated fit: {poly[0]}')
            print('individual fits:')
            print(*[f'{r.spring} {r.WABEId}: {r.discharge_model_odr1[0][0]}'
                   for _, r in ref[['spring', 'WABEId', 'discharge_model_odr1']].iterrows()],
                  sep='\n')

            p = np.linspace(-5, dpFS[wbtyp]*1.1, 100);
            q_high.D = mwabedata.outlet_diameter_o[wbtyp]
            q = ev_model(p, f_low=poly, f_high=q_high, p_threshold=dpFS[wbtyp])

            plt.figure()
            sns.scatterplot(data=ref,
                            x='Pressure_median [mbar]',
                            y=qdata, hue='spring',
                            style='WABEId')
            ax = plt.gca()
            ax.axvline(dpFS[wbtyp], ls='--', c='m', label='pressurized outlet')
            ax.plot(p, q, label='model')
            plt.legend()
            title_txt = f'Reference measurements all WABE type {wbtyp}'
            plt.title(title_txt)
            plt.gcf().tight_layout()
            plt.savefig(title_txt.replace(' ', '_') + '.png')

            reference_Q.loc[ref.index, 'joint_model'] = [poly.reshape(-1,1)] * ref.index.size
    plt.show()
    
## Plotting merged data
# df = pd.read_parquet("Bregaglia_Quelle_Löbbia_2020-2022_08.parquet")
# df.drop(index=df.index[df["Pressure [mH2O]"] < mwabedata.hQ0[3]], inplace=True)
# df["Pressure [cmH2O]"] = df["Pressure [mH2O]"] * 100
# ax, f = plot_pressure(df["Pressure [cmH2O]"], pQ0=mwabedata.hQ0[3]*100,pFS=mwabedata.hFS[3]*100, pFull=mwabedata.nozzle_zoffset[3]*100)
# df["Pressure [cmH2O]"].rolling("1d").mean().rename("daily avg.").plot(ax=ax,style=".k")
# x1, y1 =pd.to_datetime('2021-01-23 15:00'), 22.25
# x2, y2 =x1 + pd.to_timedelta('30d'), 25
# ax.annotate("~ 500 L/min", xy=(x1, y1), xycoords='data',
#            xytext=(x2, y2), textcoords='data',
#            arrowprops=dict(arrowstyle="fancy",
#                            color="0.5",
#                            patchB=None,
#                            shrinkB=5,
#                            connectionstyle="arc3,rad=0.3",
#                            ),
#            )
# ax.legend()


