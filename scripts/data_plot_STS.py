
"""
Plot data from STS sensor
"""

# Copyright (C) 2020 HSR Hochschule für Technik Rapperswil
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

from pathlib import Path
import datetime as dt

import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import scipy.optimize as spo

from pint import UnitRegistry as ureg
from pint import Quantity as Qty

#: Path to data
DATAPATH = Path('../data').resolve()

#: Data CSV format
CSVFMT = {'encoding':'latin', 'sep':';'}

#: date parser
dateparser = lambda date: dt.datetime.strptime(date, '%d.%m.%Y %H:%M:%S')

#: Options to parse a CSV file
PARSEOPT = {'skiprows': 7,
            'parse_dates': {'Datetime': ['Date', 'Time']},
            'date_parser': dateparser,
            'usecols': [3,4,5,7],
            'names': ['Date', 'Time', 'Pressure [cmH2O]', 'Temperature [C]'],
            'index_col': 'Datetime'}

h0 = 27.503 #cm, minimum water height

def parsefile(fname):
    """ Read data file """
    data = pd.read_csv(fname, **CSVFMT, **PARSEOPT)
    # Clean up. Shitty dates form datalogger
    data = data[data.index.year == 2020]
    # Remove meaningless values
    data = data[data['Pressure [cmH2O]'] >= h0] # there are at least h0 cm of water till inlet

    # The logger doesnt follow daylight saving
    ds = (data.index.month > 3) & (data.index.month < 10)
    ds = ds | ((data.index.month == 3) & (data.index.day > 29))
    ds = ds | ((data.index.month == 3) & (data.index.day == 29) & (data.index.hour >= 2))
    ds = ds | ((data.index.month == 10) & (data.index.day < 25))
    ds = ds | ((data.index.month == 10) & (data.index.day == 25) & (data.index.hour < 3))
    data.index = data.index + ds * pd.Timedelta(hours=1)

    # Somebody closed the inlet of water to the sensor
    power_off = (data.index > pd.Timestamp('2020-04-21 14:55')) \
                & (data.index < pd.Timestamp('2020-04-25 13:00'))
    data = data[~power_off]

    return data

def parseref(file):
    """ Read reference manual measurements file """
    ref = pd.read_csv(file, sep=';', skiprows=6,
                             usecols=['Datetime', 'Q1 [L/min]', 'Q2 [L/min]'],
                             parse_dates=True, date_parser=dateparser,
                             index_col='Datetime')
    q1 = ref['Q1 [L/min]'].rename({'Q1 [L/min]':'Q [L/min]'})
    q2 = ref['Q2 [L/min]'].rename({'Q2 [L/min]':'Q [L/min]'})
    ref = pd.DataFrame(q1.append(q2), columns=['Q [L/min]'])
    return ref.dropna()

def height2q(h, *, a=None, b=None):
    """ Convert cmH2O to discharge """
    #rho = 1e3 # kg/m³
    #g = 9.80665  # m/s²
#    dp = (h * 1e-2 - h0) * rho * g # Pa
    dp = (h - h0) * 0.980665  # mbar
    a = a if a is not None else 1.0
    b = b if b is not None else 39.0

    Q = np.where(dp >= 0, b * dp**a, 0.0)

    return Q

def model_error(hdata, qref, *, a=None, b=None, period=None):
    """ Evaluate model error on ref measurements """
    p_col = hdata.name
    q_col = qref.name
    err = pd.DataFrame(hdata)

    # interpolate at the time of the observations
    joint_idx = (err.index | qref.index).drop_duplicates()
    err = err.reindex(joint_idx).interpolate(
                          method='index', limit_direction='both').loc[qref.index]

    # compute model on raw pressure
    err['model'] = height2q(err[p_col], a=a, b=b)
    # merge observations & compute residual
    err = err.join(qref)
    err = err[[p_col, q_col, 'model']]
    err['residual'] = err[q_col] - err.model

    if period is not None:
        # average on period
        td_txt = f'timedelta64[{period}]'
        err[period] = (err.index - err.index[0]).astype(td_txt) 

        err = err.groupby(period).mean()

        err[f'model_{period}'] = height2q(err[p_col], a=a, b=b)
        err[f'residual_{period}'] = err[q_col] - err[f'model_{period}']

    return err

def calibrate_model(data, ref, criterion='mean', period=None):

    res_txt = 'residual'
    if period is not None:
        res_txt += f'_{residual}'

    if criterion == 'median':
        def err(ab):
            return (model_error(data, ref, a=ab[0], b=ab[1], 
                    period=period)[res_txt].abs()).sum()
    elif criterion == 'mean':
        def err(ab):
            return (model_error(data, ref, a=ab[0], b=ab[1], 
                    period=period)[res_txt]**2).sum()

    return spo.minimize(err, np.array([0.551, 93.747]), method='Nelder-Mead')

if __name__ == '__main__':

    # Reference measurements
    file = DATAPATH / 'Paliu_Fravi_ref_manual_measurements.csv'
    ref = parseref(file)
    q_col = 'Q [L/min]'
    
    # STS data
    file = DATAPATH / 'Paliu_Fravi_logger.csv'
    sts = parsefile(file)
    p_col = 'Pressure [cmH2O]'
    t_col = 'Temperature [C]'
    
    # Drop first points
    sts.drop(sts.index[:20], inplace=True)

    # time since begining
    sts['timedelta'] = sts.index - sts.index[0]
    ref['timedelta'] = ref.index - sts.index[0]
    # count days
    sts['days'] = sts.timedelta.astype('timedelta64[D]') 
    # count hours
    sts['hours'] = sts.timedelta.astype('timedelta64[h]')
    # count periods
    period='3h'
    ref[period] = ref.timedelta.astype(f'timedelta64[{period}]') 
    sts[period] = sts.timedelta.astype(f'timedelta64[{period}]') 

    # calibrate model to data
    a, b = None, None
    if ref.shape[0] > 10:
      #res = calibrate_model(sts[p_col], ref[q_col], period=period)
      res = calibrate_model(sts[p_col], ref[q_col])
      print(res)
      a, b = res.x

    # evaluate model
    sts[q_col] = height2q(sts[p_col], a=a, b=b)

    # Plot raw data
    fig, axes = plt.subplots(nrows=3, ncols=1, sharex=True, tight_layout=True)

    p = sts.plot(y=q_col, ax=axes[0], 
            linestyle='', marker='.',legend=False)

    pr = axes[0].errorbar(ref.index, ref[q_col], yerr=15, 
            linestyle='', marker='o', color='r', mfc='none')[0]

    axes[0].set_ylabel(q_col)

    sts.plot(y=p_col, ax=axes[1], 
            linestyle='', marker='.',legend=False)
    axes[1].set_ylabel(p_col)
    sts.plot(y=t_col, ax=axes[2], 
            linestyle='', marker='.', legend=False, alpha=0.5)
    axes[2].set_ylabel(t_col)
    axes[2].set_xlabel('Date')

    fig.legend([pr, p.lines[0]], ['Manual', '10min samples'],
            loc='upper center', ncol=2)

    # Show at different time scales
    fig, axes = plt.subplots(nrows=3, ncols=1, sharex=True, tight_layout=True)

    p = sns.lineplot(x=period, y=q_col,
                     markers=True, dashes=False, data=sts, ax=axes[0], ci='sd')
    p.lines[0].set_marker('o')
    p.lines[0].set_markeredgewidth(0)
    p.lines[0].set_markersize(5)

    p = sns.lineplot(x=period, y=p_col,
                     markers=True, dashes=False, data=sts, ax=axes[1], ci='sd')
    p.lines[0].set_marker('o')
    p.lines[0].set_markersize(5)
    p.lines[0].set_markeredgewidth(0)

    pr = axes[0].errorbar(ref[period], ref[q_col], yerr=15, 
            linestyle='', marker='o', color='r', mfc='none')[0]

    p = sns.lineplot(x=period, y=t_col, data=sts, ax=axes[2], ci='sd')
    p.lines[0].set_marker('o')
    p.lines[0].set_markersize(5)
    p.lines[0].set_markeredgewidth(0)

    #xtk_days = np.floor([x if x>0 else 0 for x in axes[2].get_xticks()*6/24])
    #xtk_days = np.where(xtk_days<=sts['days'].max(), xtk_days, sts['days'].max())
    #xtk_days = np.unique(xtk_days).astype(int)
    maxday = max(sts['days'].max(), ref.timedelta.astype('timedelta64[D]').max())
    xtk_days = np.arange(0, maxday + 10, 5, dtype=int)
    dt_hours = pd.Timedelta(period).total_seconds() / 3600.0
    xtk_hours = xtk_days / dt_hours * 24
    for ax in axes:
      ax.set_xticks(xtk_hours)
      ax.grid(axis='x', which='major')
    axes[2].set_xticklabels(xtk_days)
    axes[2].set_xlabel('Time [days]')
    axes[2].set_xlim(-1, xtk_hours[-1])
    fig.legend([pr, p.lines[0]], ['Manual', f'{period} average'], 
            loc='upper center', ncol=2)

    # Plot only Q
    fig, ax = plt.subplots(tight_layout=True)
    p = sns.lineplot(x=period, y=q_col,
                     markers=True, dashes=False, data=sts, ax=ax, ci='sd',
                     alpha=0.5)
    p.lines[0].set_marker('o')
    p.lines[0].set_markersize(5)
    p.lines[0].set_markeredgewidth(0)
    pr = ax.errorbar(ref[period], ref[q_col], yerr=15, 
            linestyle='', marker='o', color='r', mfc='none')[0]

    ax.set_xticks(xtk_hours)
    ax.grid(axis='x', which='major')
    ax.set_xticklabels(xtk_days)
    ax.set_xlabel('Time [days]')
    ax.set_xlim(-1, xtk_hours[-1])
    fig.legend([pr, p.lines[0]], ['Manual', f'{period} average'], 
            loc='upper center', ncol=2)

    plt.ion()
    plt.show()

    fout = '../../data/Paliu_Fravi.csv'
    sts.to_csv(fout, encoding='utf-8', sep=';')
    print(f'Saved merge to {fout}')
