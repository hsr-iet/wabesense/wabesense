========================
Datalogger Installation
========================

.. contents:: :local:


Firmware
--------
Install a basic firmware consists of the Bootloader and a WABEsense-Application on the data logger unit. Follow the steps below together with `ST-LINK/V2`_ & `STM32CubeProg`_ to complete this step:

Preparation
^^^^^^^^^^^

	- Connect a power source (Battery/External Power: `J1`, USB: `J2`) to the datalogger
	- Connect the programmer (`ST-KINK/V2`) to the JTAG connector `J4` on the datalogger
	- Run the `STM32CubeProg`_ application
	- Select ST-Link as used Programmer on the right top side of the application
				
		.. image:: ../pictures/STM32_CubeProgrammer_ST_Link.JPG
		  :width: 250
		  :alt: Datalogger verification: STM32CubeProg ST Link selection.JPG
		
	- Select "Erasing & Programming"-Tab on the left side
		.. image:: ../pictures/STM32_CubeProgrammer_Erase_Programming_Tab.JPG
		  :width: 50
		  :alt: Datalogger verification: STM32CubeProg Erase and Programming Tab.JPG
		  
Bootloader programming
^^^^^^^^^^^^^^^^^^^^^^

	- | Click "Browse" to locate file from datalogger_firmware-Repo:
	  | datalogger_firmware/CodeBootloader_STM32L496RG/Release
	  | WABEsense_Datalogger_STM32L496RGT6_Bootloader.bin
	  |
	  | Note: This file must first be generated with `STM32CubeIDE`_ (Release setting)
	- Set Start address to: 0x08000000 (default)
	
	.. image:: ../pictures/STM32_CubeProgrammer_Bootloader_programming.JPG
	  :width: 500
	  :alt: Datalogger verification: STM32CubeProg Bootloader programming.JPG
	  
	- Press "Start Programming"
		
Application programming
^^^^^^^^^^^^^^^^^^^^^^^
	- | Click "Browse" to locate file from datalogger_firmware-Repo:
	  | datalogger_firmware-Repo -> datalogger_firmware/Code_STM32L496RG/Release
	  | WABEsense_Datalogger_STM32L496RGT6.bin
	  |
	  | Note: This file must first be generated with `STM32CubeIDE`_ (Release setting)
	- Set Start address to: 0x08020000
	
	.. image:: ../pictures/STM32_CubeProgrammer_Application_programming.JPG
	  :width: 500
	  :alt: Datalogger verification: STM32CubeProg Application programming.JPG
	  
	- Press "Start Programming"

Hardware
--------
The circuit board passed an electrical test during production. Other tests are performed during the PCB assembly work workflow (SMD inspection, X-ray inspection, and final inspection).

**Note**: key labels correspond to hardware version 1.0

.. warning:: Only carry out hardware manipulations when no power source is connected to the datalogger (External Power: `J1`, USB: `J2`)!

Execute all possible user-interactions with the datalogger and check their interactions.


Powered/connected by USB cable to PC/Laptop only
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
The datalogger is only powered by USB cable, which is connected to the PC/Laptop. The CR2032 batterie is inserted.

.. list-table:: Powered/connected by USB cable to PC/Laptop only
	:widths: 1 10 50 100
	:header-rows: 1
	
	* 	- #
		- check
		- expected result
		- possible errors
	* 	- 1.
		- System-Check
		- red LED turns on for one second (battery voltage out of range because not connected)
		- no LED signalisation
		
			--> check LED cabling
	* 	- 2.
		- Manual measurements
		- blue/green LED turns on during measurement
		- red LED turns on after measurement
			
			--> BME280 Sensor timeout/issue
	*	- 3.
		- Measurement data verification
		-	- BME280 environment data are in expected range
			- Analog input voltages are in expected range
		- depends on values out of expected range
		

Powered by batteries only
^^^^^^^^^^^^^^^^^^^^^^^^^
The datalogger is only powered by batteries. No USB-Cable is inserted. The CR2032 batterie is inserted.

.. list-table:: datalogger powered by batteries only
	:widths: 1 10 50 100
	:header-rows: 1
	
	* 	- #
		- check
		- expected result
		- possible errors
	* 	- 1.
		- System-Check
		- green LED turns on for one second
		- 	- | yellow LED signalisation
			  | --> Battery needs to be replaced (or wrong configuration of battery (voltage and/or number of cells))
			- | red LED signalisation
			  | --> Battery empty (or wrong configuration (voltage and/or number of cells))
			- | no LED signalisation
			  | --> check LED cabling
	*	- 2.
		- Manual measurement execution
		- yellow LED turns on for short time and turns off afterward
		- | red LED turns on after yellow for short time (hardware error/timeout)
		  | --> Try to export log-data and see activity-log for more details
	*	- 3.
		- Data-Export via USB-Stick execution
		- 	- yellow LED blinks with 10Hz
			- Log files are exported to USB Stick (check them afterwards with laptop/pc)
			- green LED turn on for one second to mark successful execution
		- red LED turn on for one second because of an error/abort
			- USB Stick not detected
			- | File(s) could not be written to USB Stick
			  | --> Check USB Stick or try with another one


RTC Battery
^^^^^^^^^^^

**Note**: key labels correspond to hardware version 1.0

For the integrity of the configuration data, a 3.0V CR2032 battery should be installed in the `BT1` battery holder.
This power source also keeps the RTC running when no other power source is connected to the datalogger.
Expected life time, even without other power sources is around 10 years.


Links
=====
.. _ST-LINK/V2: https://www.st.com/en/development-tools/st-link-v2.html
.. _STM32CubeProg: https://www.st.com/en/development-tools/stm32cubeprog.html
.. _STM32CubeIDE: https://www.st.com/en/development-tools/stm32cubeide.html

.. target-notes::
