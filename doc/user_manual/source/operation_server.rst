===========================
Server Operation
===========================

.. contents:: :local:


Server
======

Description of how to operate the server

Dashboards
----------

.. todo::
	Dashboards JP


Certificates
------------
.. todo::
	Influx and Grafana

Influx DB
------------
.. todo::
	- Run Flux queries
	- Https server

CLI tools
---------

.. todo::
	CLI tools JP

- List tunnels
- Important folders
- Backup data

Maintenance
-----------

- Update nodes
- OS updates
- Software updates

