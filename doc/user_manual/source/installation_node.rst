====================================
Node Installation
====================================

.. contents:: :local:

Description of the configuration of the reporter.

Currently WABEsense Nodes are Raspberry Pi 4 model B (4GB RAM).
The installation of the node consist of the following steps:

1. Install the operative system (OS) into a microSD card that will be inserted into the Raspi.
2. Configuration of the OS to be a WABEsense node
3. Creation and authorization of the user in the server
4. Registering the node into the server


OS image
========

.. image:: ../pictures/RaspberryPi_Imager_MainScreen.png

- Use imager https://www.raspberrypi.com/software/

- Use latest image: Raspberry Pi OS LITE (64-bit)

- Storage: Micro SD

In te configuration menu (bottom-right gear button)

- Set to use SSH

- Set username ``pi`` 

- Set password ``raspberry``-

- Save the config

.. image:: ../pictures/RaspberryPi_Imager_ConfigScreen.png

Now click on the button ``Write``.

When the process is finished (it will write the image and run a verification): 

- Make sure the SD-card has two partitions: ``boot`` and ``rottfs``.
  If they are not present redo the previous steps.


If you forgot to add SSH to the configuration:

- Add an empty ``ssh`` file to the ``boot`` partition of the OS SD-card using a computer.
  You can do this by opening a terminal on the mounted ``boot`` partition and running::

    touch ssh

If you forgot to add username and password to the configuration:

- Add the ``userconf.txt`` file to the ``boot`` partition of the OS SD-card using a computer.
  You can do this by opening a terminal on the mounted ``boot`` partition and running::

    echo "pi:$6$9i8PP2LgVBwDqMS1$1gNrsD3HRIeK9uqO0ujfWAKiKUgkrCiivscwbzpVX9XGKf95Xxx5wLW4Tyc7FT7aKnbHXWs9wKfpKE5AaJ8oW1" > userconf.txt

  This will allow you to login via ssh with username ``pi`` and password ``raspberry``.


Installing requirements
========================
Cconnect the pi to the network and log in via ssh to it with username "pi"
::

    ssh -o"UserKnownHostsFile=/dev/null" -o"StrictHostKeyChecking=no" pi@<raspi ip>

To detect the ip of the raspi in the local network try
::

    sudo nmap -sn 152.96.218.* | grep -i -B2 raspberry

adapt network accordingly.

Update and upgrade
::

    sudo apt update && sudo apt full-upgrade -y

Install required tools
::

    sudo apt install -y git

Raspberry OS setup
===================

#. Log in to the raspberry.

#. Change user ``pi`` password using project's secrets (the file is found in the shared disk).
   To do this, execute ``passwd`` as pi user.

#. Clone the ``wabesenclient`` repository then go to it::

    git clone https://gitlab.com/hsr-iet/wabesense/wabesenclient.git && cd wabesenclient

#. To configure the OS run::
   
    sudo ./initial_setup.sh <reporter id>
    
.. warning::
    When these changes are applied remote login to the raspi needs authorized keys. 
    Make sure your key is authorized, check that it appears in the list printed by the command::

        cat ~/.ssh/authorized_keys
        
#. Upload public ssh key to ``wabesense`` repository.
   The public key is found by running: ``cat ~/.ssh/id_rsa.pub``.
   A file named ``<reporter id>.pub`` containig this key neeeds to be added to the folder ``resources/ssh_keys/reporters/`` of the wabesense repository.
   Then use git to add the key file and push it.

   .. note::
      If the file ``<reporter id>.pub`` already exsit and you are sure you want
      a node with this reporter id, then **append** the key to the already existing file.

   To retrieve the public key from the raspi remotely, use a pc with access to it::

    scp pi@<raspi ip>:.ssh/id_rsa.pub <path to wabesense repository>/resources/ssh_keys/reporters/<reporter id>.pub
    

#. Reboot: ``sudo reboot``

Reporter setup at server
========================

New reporter
------------

If the ``<reporter id>`` used in the previous step was new follow these steps:

1. Log in to the server ``wabesense.ch``

2. If not done yet, install the ``wabesenserver`` repository in a virtual environment::

    python -m pip install -U git+https://gitlab.com/hsr-iet/wabesense/wabesense_server.git@release

3. run the reporter creation script::

    wabesense_create_reporter.py <reporter id> <influx token>

   The ``<influx token>`` corresponds to the reporter's token in the output of the command::
   
    influx auth list

After these steps the reporter username is authorized in the server.

.. note::
   This steps requires the public key of the reporter to be available in the
   wabesense repository as explain in the steps of the previous section.
   
Already existing reporter
-------------------------
If the reporter already exited as a user in the server then do the following steps:

1. Log in to the server ``wabesense.ch``
2. Append the new public key to the file ``/mnt/wabesense_data/reporters/<reporter id>/.ssh/authorized_keys``

After these steps the node using an existing reporter username is authorized in the server.

Remote reporter setup
=====================

1. Log in to the raspberry and cd to ``wabesenclient``

2. Link the node with the reporter in the server::

    sudo ./setup_as_repoter.sh <reporter id>

After this step the node should appear in the home panel of Grafana https://www.wabesense.ch:3000

