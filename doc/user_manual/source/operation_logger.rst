===========================
Datalogger operation
===========================

.. contents:: :local:


This chapter describe the different situations of how to operate with the datalogger.
  * `Configuration`_ via Serial user interface
  * interaction via `Human machine interface`_


Configuration
=============

A serial user interface is used to setup/configure the datalogger. 
All details of the used protocol in this interface is described in the public repository `WABEsense/dataloggerUI`_ Reference Manual.

The configuration is set over the USB-Interface of the datalogger. 
A USB-C cable between the datalogger and a laptop/PC is needed, to setup the communication between them (virtual com port).
Possible configuration parameters are:

  * Device name
  * Analog channel name
  * battery configuration
  * sampling interval
  * RTC Date/Time
  * Auto Sampling ON/OFF


Re-configuration
------------------

The re-configuration is simple the same as a confiuration. Just for sure, all the previous log-data should be cleaned up on the datalogger. 
This is done with the "data_clear" action.


.. _installation-naming-convention:

WABEsense Naming convention
=============================
**Note**: key labels correspond to firmware version `0.5.0` or later

The objective of the naming convention is to send as much metadata about the logger and its situation
as possible [#reason]_.
The WABE object needs to be identified to know what design parameters corresponds to it [#wabeid]_.

After surveying the WABE ID in several locations we found out that the WABE ID is not unique.
The WABE ID is related to the project number used by ROMAG, and one project might produce many WABE.

.. code-block:: yaml

    Device name: <location>.<building>.<spring>
    Analog channel mappings:
        <HW channel>: <wabeID_wabeType>.<sensor ULAG label>.<physical magnitude>(<SI units>)

Example:

.. code-block:: yaml

  Device name: Oberriet.SS.Ulrika
    Analog channel mappings:
        ADC_IN8 (HW): AB12641-002_2.DT07.pressure(V)
        ADC_IN9 (HW): AB12641-002_2.DT07.temperature(V)


The `building` can be :
 - `SS` for **Sammelstube** (also Sammelschacht and Samelbrunnenstube)
 - `BS` for **Bunnenschacth** (also Brunnenstube)
 - `RES` for **Reservoir**

If `<spring>` is meaningless (e.g. there is no spring name) then use any identifier used by the operator (e.g. a number).
If that is not available then use the the ID of the WABE.
For example, in Zug the WABE `AB01658-00` (I added a hyphen for the sake of illustrating the convention) is used to
collect water from many springs. It is a type 4 WABE. In this case then we would have

.. code-block:: yaml

    Device name: Zug.SS.AB01658-00_4
    Analog channel mappings:
        ADC_IN8 (HW): AB01658-00_4.DT02.pressure(V)
        ADC_IN9 (HW): AB01658-00_4.DT02.temperature(V)

A separate document should detail the situation of this WABE, e.g. whether there is no name for the spring, or what
springs are being collected, etc.

If the datalogger is connected to sensors in more than one WABE, then join the spring names with a comma `,` (no space).
Then decorate the channel name with the ordinal position of the spring (startign with 0) to which it is connected.
For example, in `Bonaduz` we could have a datalogger collecting data from sensor `DT11` connected to the spring
`Paliu Fravi` (wabeID `AB14494` type 2) and `DT12` connected to the spring `Leo Friedrich` (wabeID `AB02287` type 3).
The Naming in this case should be:

.. code-block:: yaml

    Device name: Bonaduz.SS.Paliu_Fravi,Leo_Friedrich
    Analog channel mappings:
        ADC_IN8 (HW): 0.AB14494_2.DT11.pressure(V)
        ADC_IN9 (HW): 1.AB02287_3.DT12.pressure(V)
        ADC_IN10 (HW): 0.AB14494_2.DT11.temperature(V)
        ADC_IN11 (HW): 1.AB02287_3.DT12.temperature(V)

Note that spring names composed of several words are joined using an undercore (`_`), e.i. replacing spaces by underscores.

.. rubric:: Footnotes


.. [#reason] The naming convention would be simplified, or even unnecessary, if the logger's used the SD-card to store
             metadata of the site.


.. [#wabeid] Originally it was thought that the WABE type would be enough, but the objects in the field do not follow a
             single type design. Then it was thought that the WABE ID given by the objects' producer was enough, but the
             producer does not use an unique ID per object but per project number, and project might produce many objects.
             Currently we are using a combination of spring name, WABE ID, and WABE Type to uniquely identify the object.
             This however, might break down if the object relocated to anew spring, as we would see it as a new object,
             but it actually it was just connected to a different spring.


Human machine interface
========================

.. image:: ../pictures/datalogger_zeroSeries_annotated.png
  :width: 400
  :alt: Datalogger zero series product
  
Red User-Button: System operation
Green user-Button: Data operation

Manual-Measurement operation
----------------------------

Thick lines means default/normal workflow.

1. Generate "Manual-Measurement Event" with green Button
2. The following process will be executed

.. mermaid::

  graph TD
  A[Turn 17V Sensor Powersupply on<br/>Turn yellow LED ON<br/>Setup & start sampling of BME280 Sensor<br/>get RTC-Time<br/>loopCnt = 0] ==> B[analog Input setup]
  B ==> C(measure battery voltage<br/>increment loopCnt)
  C ==> D{loopCnt >= 4}
  D ==>|yes| E(calculate average battery voltage value)
  D ==>|no| C
  E ==>F{17V_PowerUp time expired}
  F -->|yes| G(loopCnt = 0)
  F ==>|no| H(System to sleep for 17V_PowerUp time left)
  H ==> |system sleep exit|G
  G ==> I(measure all analog channels<br/>system sleep for 100uSeconds<br/>increment loopCnt)
  I ==> J{loopCnt >= 4}
  J ==>|yes| K(calculate average values<br/>Turn 17V Sensor Powersupply off)
  J ==>|no| I
  K ==> L{BME280 measurement time expired?}
  L ==>|yes| M(store measurment values in FLASH)
  L -->|no| N(System to sleep for BME280 measurement time left)
  N --> |system sleep exit|M
  M ==> O{stored successfully and not a BME280 timeout?}
  O ==>|yes| Z(exit - LED off, system to standby)
  O -->|no| Y(Show up Error: red LED on)
  Y --> |10 millisecond timeout|Z

.. note::
  Because of Standby-Mode of Microcontroller and most of the System-Parts, there are some minimal timings need to be respected after a wakeup from standby.

  - The used external sensor has a power-up Time of 200ms
  - | BM280 Sensor has as well a minimal time to measure and convert the physical channels, but this is faster then the power-up time of the external sensor
    |
    | Current setup is: Temperature, Pressure and Humidity are all working with an Oversampling factory by 16
    | 
    | This gives a total time of t=1.25ms + (2.3 * T_oversampling) + [(2.3 * P_oversampling) + 0.575] + [(2.3 * H_oversampling) + 0.5] = 112.725ms

.. _system-check:

System-Check operation
------------------------

Thick lines means default/normal workflow.

1. Generate "SystemCheck Event" with red Button
2. The following process will be executed

.. mermaid::

  graph TD
    A[check Battery configuration, measure battery voltage] ==> B{check voltage against thresholds}
        B -->|mark as empty| C(check BME280 sensor)
    B -->|mark as to be replaced| C
    B ==>|mark as good| C
        C ==> D{BME values in expected range?}
        D -->|out of limits| Y(Show up Error: red LED on)
        D ==>|inside limits| E(Show up battery marking)
        E -->|empty mark| Y
        E -->|to be replaced mark| W(Show up battery replacment: yellow LED on)
        E ==>|good mark| X(Show up battery okay and complete success: green LED on)
        W --> |1 second timeout|Z
        X ==> |1 second timeout|Z(exit - LED off, system to standby)
        Y --> |1 second timeout|Z

Data-Export operation
-----------------------

Thick lines means default/normal workflow.

1. Insert USB-Stick
2. Generate "dataExport Event" with USER-Buttons: press both Buttons together (withhin 50ms)

3. The following process will be executed

.. mermaid::

  graph TD
    A[USB-Stick inserted] ==> |dataExport Event| B(export log-Data, yellow LED blink with 10Hz)
    B ==>|dataExport done| C{successful data export?}
    C ==>|successful| X(Show up success: green LED on)
        C -->|fail| Y(Show up Error: red LED on)
        X ==> |1 second timeout|Z(exit - LED off, system to standby)
        Y --> |1 second timeout|Z

Power-Up operation
------------------

If all power sources, including the CR2032 backup battery is removed from the device and the system is started up by connecting a valid 
power source, first there is a internal process to restore all data-cursors. During this time, the red LED blinks with 10Hz. This 
step takes a couple of seconds, up to some minutes. Wait until this step has finished and the green LED appears for 1 second. After 
this time, the logger is ready for other operating interactions (this includes also the serial interface, which is blocked during cursor restoring).

Maintenance
===========

Firmware updates
-----------------

A firmware update can be carried out using an USB-Stick. The new firmware-file should be installed in the Top directory on the
USB-Stick. After installing the USB-Stick in the datalogger and executing the "data-export"-operation, the current measurement data is saved on the USB-Stick, 
followed by the firmware update being checked. If a firmware file is found, the bootloader carries out the firmware update. During the update, the red LED flashes 
at 10 Hz. If an error occured, the red LED lights up for one second.
Afterwards, the bootloader jumps to the newly installed firmware and start with the execution of it (yellow LED still blinky).
This ending process-step contains the generation of a log-activity and afterwards a second export of all new data (mainly the new activity-log) to the USB-Stick. 
If this step is successsfull as well, the green LED will turn on for one second. If the data-export fails, the red LED will turns on for one second.
The USB-Stick could be removed and the device is working on in the same configuration like as before the Firmware update.

Thick lines means default/normal workflow.

.. mermaid::

  graph TD
    A[USB-Stick inserted] ==> |dataExport Event| B(export log-Data, yellow LED blink with 10Hz)
    B ==>|dataExport done| C{successful data export?}
    C ==>|successful| D(Check if a valid firmware File exists on USB-Stick)
    D ==> E{firmware file found?}
        E ==>|Yes| G(Enter bootloader: red LED toggle with 10Hz)
        E -->|No| H(Successfull data export, but no firmware update)
        G ==> I(firmware update process: red LED toggle with 10Hz)
        H --> X(Show up success: green LED on)
        I ==> J{firmware update successful?}
        J -->|failed| W(System maybe not working anymore!)
        J ==>|successful| K(export log-Data)
        K ==>|dataExport done| L{successful data export?}
        L ==>|successful| X
    L -->|failed| Y
        W --> Y(Show up Error: red LED on)
        C -->|failed| Y
        X ==> |1 second timeout|Z(exit - LED off, system to standby)
        Y --> |1 second timeout|Z

Naming convention
*****************

.. code-block:: yaml

  The firmware file must be named: datalogger_firmware.bin


Links
=====
.. _WABEsense/dataloggerUI: https://hsr-iet.gitlab.io/wabesense/dataloggerui/

.. target-notes::
