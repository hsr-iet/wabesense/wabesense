=====
Links
=====

.. contents:: :local:

Other reference manual
======================

Hardware Reference Manual
	- https://hsr-iet.gitlab.io/wabesense/datalogger_hardware/index.html
	
Firmware Reference Manual
	- https://hsr-iet.gitlab.io/wabesense/datalogger_firmware/index.html
	
User Interface Reference Manual
	- https://hsr-iet.gitlab.io/wabesense/dataloggerui/index.html