===============
Troubleshooting
===============

.. contents:: :local:

Datalogger
==========

System PowerUp
--------------

red LED blinks (with 10Hz)
    This is normal, if system was powered up from zero. That is, before powering up the logger
    did not have any power connected to it (including the RTC battery). 
    During this time, the data cursors are restored. Wait until it is finished, 
    it takes about 2 minutes. At the end, the LED will shine green for one second.
    After this, the logger is ready for operation and all stored measurments 
    can be read out.

Unknown issue
-------------

- Always check the log-file if able to export this file. 
  This file contains readable history events about the datalogger.
- If possible, execute a :ref:`System-Check <system-check>`.

