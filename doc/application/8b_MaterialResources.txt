4. Welche personellen und materiellen Mittel stehen den Projektpartnern zur Verfügung? (z.B. Anzahl Mitarbeiter am Institut/im F&E-Bereich, bereits vorhandene Geräte, usw.)

What human and material resources are available to the project partners? (e.g. number of employees at the institute/ R&D department, already existing equipment, etc.)


HSR-IET has 36 scientific and project staff members, 12 of whom are simulation experts with profound knowledge in:
- Computational Fluid Dynamics: Ansys CFX, Ansys Fluent, StarCCM+, COMSOL, OpenFOAM, Palabos, EDEM
- Finite Element Method: Ansys Mechanical, FENics
- Coupling of different software tools (multiphysics): StarCCM+, COMSOL
- Data science and systems modeling: Python, GNU Octave, R, Julia, Matlab
 
In addition, HSR-IET has a well-developed network of contacts to research institutes, colleges and universities and is active in the working group for multi-physics and high-performance computing at the International Association for the Engineering Modelling, Analysis and Simulation (NAFEMS).
Due to the in-house computing cluster and the close cooperation with Microsoft, the IET has a practically unlimited amount of computing power at its disposal thanks to Microsoft Azure, which could be useful for numerical modeling.
HSR also maintains its own HPC resources customized for large FEM simulations. The cluster at the HSR that will be available for the project has 24 nodes and 480 cores with 2.3 TB RAM, plus a fat-node with 24 cores, 512 GB RAM, and GPU 1x Tesla V100. 
 
ULAG has more than 40 years experience in the field of drinking water and with water utilities. Over these years it has grown a vast network and a solid understanding of the market. As one of the leading companies in the drinking water industry, the employees of ULAG have a large know-how in the field of drinking water. 
In the course of the project Uli Lippuner, one of the pioneer of in the Swiss drinking water industry, will be the first contact with the water utilities. This will be followed up by the corresponding agreements with the water utilities, which will be settled by the managing director of ULAG, Daniela Guardia, and the head of legal water affairs, Jeannette Lippuner. Both of them already have proven know-how in this field, and they also maintain very good relations with all the water utilities target by the project. Water utilities have repeatedly stated that the infrastructure and services provided by ULAG are above average, in particular because they are designed to be sustainable, well-considered and long-lasting. Finally, Daniel Ruf, will carry out the installations of sensors and data-loggers.  Daniel has many years of of experience with installations and commissioning of plants, and plays the role of lookout within ULAG: looking for and identifying unsatisfied needs on sites.
ULAG will put at disposition of the project the following materials:
- Company car
- Plotter
- State-of-the-art IT infrastructure, including own computers and servers
- Tools for sensor and data-logger installation
 
HSR-IMES will offer all necessary tool and equipment for ASIC and sensor design, PCB designs, and its various embedded systems platforms.
Among other equipment this include:
- Soldering station
- 3D printers
- Electric testing platforms
- PCB desing software
