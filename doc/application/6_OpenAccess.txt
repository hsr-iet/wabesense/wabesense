6. Gemäss Umweltschutzgesetz (SR, 814.01) Art. 49 Abs. 3 müssen die Finanzhilfen der Umwelt-
technologieförderung bei einer kommerziellen Verwertung der Ergebnisse zurückerstattet
werden. Die Verpflichtung zur Rückerstattung erstreckt sich auf 10 Jahre nach Abschluss des
Projektes. 

Wie beabsichtigen Sie die hier beantragten Finanzhilfen zurückzuerstatten ? Welche
Projektpartner/innen wären von dieser Verpflichtung betroffen?

Falls die Ergebnisse der Öffentlichkeit kostenlos zur Verfügung gestellt werden, können Sie sich
von der Rückzahlungspflicht befreien lassen. Wenn Sie von dieser Möglichkeit Gebrauch machen
wollen, beschreiben Sie bitte nachfolgend, wie die Ergebnisse nutzbar gemacht werden sollen.

According to the Environmental Protection Act (SR, 814.01) Art. 49(3), financial assistance from the environmental technology funding is refunded in the event of commercial exploitation of the results will be. The reimbursement obligation extends for 10 years after completion of the project. 

How do you intend to reimburse the grants requested here ? Which Would project partners be affected by this obligation?

If the results are made available to the public free of charge, you can from the obligation to repay. If you make use of this option please describe below how the results are to be used.


The three main products of the project are meant to be freely distributed to the public.

- Open hardware data-logger: design documents, firmware, and software will be publicly available under suitable permissive licenses (CC-BY-SA, GPL) following the best practices of the Open Source Hardware Association [OSHWA]

- Open software pipeline: all software tools will be developed using libre and open source software (Python, OpenFOAM, GNU Octave). They will be freely available under suitable permissive licenses (GPL).

- Availability of measurements: this is the only item that we cannot ensure in its totality.
  During T3.1 Agreements, we strive to identify and overcome legal barriers limiting the amount of shareable data.
  During the conception of the project we could not find a national network for the storage and distribution of these type of datasets.
  Hence, we allowed by agreements, we will use data repositories maintained by the SWITCH foundation, in particular the SWITCHengines service. We will also share selected and curated datasets using non-profit organization services (e.g. Zenodo, OSF). 

Software and hardware designs will be shared via public repositories, e.g. GitLab, GitHub, etc.
