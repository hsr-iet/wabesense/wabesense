5. Leistet die neue Technologie auch Beiträge im Hinblick auf die sozialen und wirtschaftlichen
Aspekte der nachhaltigen Entwicklung? Wenn ja, welche?

Does the new technology also contribute to the social and economic development of the country?
aspects of sustainable development? If so, which ones?


The assessment of a spring is only possible if the discharge behavior of the spring is known year-round over several years.
This helps characterizing the spring, and decide if it fulfills the needs of the water supply.
This knowledge also facilitates more reliable statements about existing spring captures, ultimately allowing for monetary investments decisions based on real data and not only on estimates and assumptions.
Adequate investments further sustainability.
In particular, spring rehabilitation requires investments that will have to be amortized in the future.
Hence the ability to decide whether a spring should be rehabilitated or its use discontinued is of central importance to the economy of the water utility.
With a proper spring assessment, it is also possible to prevent the construction or renovation of buildings that have little or no benefit in the short to medium term.
Furthermore, the discharge of a spring is necessary to properly compute the construction or maintenance costs per liter of water, that can result in the shutdown of a poor spring [Lippuner2018].

At the national economic level, we believe that non-captive markets are the road to sustainability.
By placing a needed open hardware solution without vendor lock-in in the market, we will foster higher competition and the iterative improvement of the technology.
The consequent diversification of the market is also a quantifiable general benefit.
Most public enterprises and many private companies do not see the value and advantages of process based on open software and hardware.
The current project will set a precedent in this regard and contribute to the widespread of these type of solutions.

Ultimately publicly available spring discharge charts will elevate discussions about alpine water availability to a more quantitative level; and allow the participation of the interested general public.

References

[Lippuner2018] Lippuner, U. (2018). Quellwasser als natürliche Ressource, Technische und bauliche Rahmenbedingungen zur Quellfassung (pp. 248). LIPartner AG Verlag. ISBN 978-3-907926-73-4. Retrieved 12.04.2020 https://www.lipartner.ch/de/publikationen/fachbuch-quellwasser/

