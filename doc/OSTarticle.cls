\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{OSTarticle}

% Assume that any passed option will be understood by the article class
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{article}}
\ProcessOptions \relax

\LoadClass[a4paper,11pt]{article}
\pagestyle{plain}

\RequirePackage{authblk}
\RequirePackage[latin1,utf8]{inputenc}
\RequirePackage[dvipsnames, svgnames]{xcolor}
\RequirePackage{fancyhdr}

% Some required for science
\RequirePackage{amsmath}
\RequirePackage{siunitx}
\RequirePackage{graphicx}


\setlength{\textwidth}{15cm}
%\setlength{\headheight}{25pt}
%\setlength{\headsep}{2cm}
%\setlength{\footskip}{2.8cm}
\setlength{\textheight}{22.0cm}
\setlength{\oddsidemargin}{0.46cm}
\setlength{\evensidemargin}{0.46cm}
\setlength{\parskip}{1ex}
\setlength{\parindent}{0cm}


%\pagestyle{empty}
\pagestyle{fancy}
\fancyhf{}
\rhead{\vspace{-1cm}\begin{minipage}{3cm}\includegraphics[scale = 0.8]{OSTlogo.png} \end{minipage}}
\cfoot{\thepage}
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0pt}

\renewcommand\maketitle{ 
  \newpage
  \null
  \vskip 2em%
  \begin{center}%
  \let \footnote \thanks
    {\LARGE \@title \par}%
    \vskip 1.5em%
    {\large
      \lineskip .5em%
      \begin{tabular}[t]{c}%
        \@author
      \end{tabular}\par}%
    \vskip 1em%
    {\large \@date}%
  \end{center}%
  \par
  \vskip 1.5em
}

