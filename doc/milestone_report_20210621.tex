\documentclass[10pt, a4paper]{OSTarticle}
\input{include/article_basics.tex}

\hypersetup{colorlinks = true}

\graphicspath{{img/}}

\title{Project WABEsense - Digitalisierte Brunnenstuben\\Milestone report (milestone 1)}
\author[1]{Juan Pablo Carbajal}
\affil[1]{OST - Institute for Energy Technology. Scientific computing~\&~engineering group}
\author[2]{Adrian Tüscher}
\affil[2]{OST - Institute for Microelectronic and Embedded Systems}

\begin{document}

\maketitle

\section{Executive summary}
This report summarizes the deliverables that lead to the completion of the first legal milestone in the  WABEsense project (Contract nr. UTF 642.21.20 / BAFU-D-0A3D3401/199).
The milestone includes the completed design of the Zero-series logger and the tool to generate CFD-based discharge curves of water tappings, including all WABE designs.

The zero-series logger is an improvement on the alpha-series exploiting the experience the project collected from the alpha sites. 
The hardware design and firmware are publicly available and distributed under free and libre hardware and software licenses.

The discharge curve of water tappings can be generated using the CFD Q-sense module and OpenFOAM.
The project is using this module to generate the discharge curve of all WABE designs.
Q-sense is also being used for other case studies within the project, establishing the general usability of the module.
The module is documented, publicly available, and distributed under free and libre software licenses.

More information about the project, e.g. photos, can be found in the \href{https://hsr-iet.gitlab.io/wabesense/blog/}{WABEsense's blog}.

\paragraph{Note:} as of the date of this report, the COVID pandemic has produced delays and unavailability of electronics, which in turn is delaying the deployment of the zero-series.
In particular the micro-controller is scarcely available and the logger housing box was delayed.

\subsection{Highlights}
\begin{itemize}
  \item Zero-series logger design is complete.
  \item Discharge curves for all WABE designs can be generated automatically using the CFD Q-sense module and OpenFOAM.
  \item All deliverables (logger design and software) are publicly available and distributed under copyleft licenses.
\end{itemize}

\section{Data-logger}

The data-logger is a critical component of the digitalization process which records the measurements of an external pressure sensor installed in the WABE, and allows the user to extract the data using portable media, like a USB stick or an SD card.
Fig.~\ref{fig:record_process} shows the components involved in the measurement and logging process: pressure sensor, and data-logger (alpha-series).
Details and lessons learned during the first installations are recorded on the \href{https://hsr-iet.gitlab.io/wabesense/blog/feldinstallationen.html}{project's blog}.

\begin{figure}[h]
  \centering
  \includegraphics[height=10cm]{wabesense_installation_ulrika_annotated.png}
  \caption{\label{fig:record_process}. Components involved in the measurement and logging process: pressure sensor, and data-logger (alpha-series).}
\end{figure}

The logged data can be then uploaded to an online database via a communication node (currently a Raspberry Pi computer) installed at the user side.
Together with the raw data, the computed discharge is added to the database at this point of the process.
The contents of the database are then visualized on a Grafana dashboard available on the internet.
The dashboards show the data relevant to the well, and also measurements of the state of the logger: pressure, temperature and humidity inside the housing, as well as battery voltage.
Fig.~\ref{fig:dashboards} shows the two dashboards.
The water production dashboard shows the discharge signal over time (panel "discharge"), a summary of the values in a table (panel "summary"), and a histogram of discharge duration (panel "discharge duration").
The logger dashboard shows the temperature, pressure, and relative humidity inside the loggers housing.
It also shows the battery voltage in a fourth panel.
Details about the data infrastructure and future plans are described on the \href{https://hsr-iet.gitlab.io/wabesense/blog/data-infrastructure.html}{project's blog}.

\begin{figure}[p]
  \centering
  \includegraphics[width=0.95\textwidth]{wabesense_dashboards_waterproduction.png}
  \includegraphics[width=0.95\textwidth]{wabesense_dashboards_logger.png}
  \caption{\label{fig:dashboards} WABEsense Grafana dashboards. A summary of the well water production (top), and the internal state of the logger (bottom).
  The water production dashboard (top) shows the discharge signal over time (panel "discharge"), a summary of the values in a table (panel "summary"), and a histogram of discharge duration (panel "discharge duration").
The logger dashboard (bottom) shows the temperature, pressure, and relative humidity inside the loggers housing.}
\end{figure}

The first 5 loggers were installed on January-February 2021, and have been collecting data since then without major troubles.
The noise level in the data has been stable, showing no signs of degradation.
The battery level has been also stable despite high humidity levels.
The logger in well Leo Friedrich (WABE AB02287, green on the logger dashboard), was temporarily submerged under water due to an accident.
The battery showed degraded voltage shortly after the event, but has since then been on recovery.
The recorded data showed no degradation despite the direct exposure to water.

\subsection{Zero-series logger}
The list below summarizes the most important changes that were introduced in the zero-series logger:

\begin{itemize}
\item Changing the micro-controller from STM32L412 to STM32L475 to support the USB-C protocol.
\item Serial interface, data transfer, and upgrade over USB-C port
\item Adding more sensor connectors to use the 4 analog channels available
\item Non-soldering connectors for buttons and LED
\item Connector used by ST for the programmer/debugger device
\item Discarding the integrated battery support
\end{itemize}

The new version holds an ultra-low-power micro-controller with USB OTG interface of the \href{https://www.st.com/en/microcontrollers-microprocessors/stm32l475re.html}{STM32L475xx} family and is currently aimed at logging scenarios with low sampling rates.

The data-logger is Open Hardware and can be reused and modified under the terms of the \href{https://creativecommons.org/licenses/by-sa/4.0/}{Creative Commons Attribution-ShareAlike 4.0 International} license.
The development files are available on the \href{https://gitlab.com/hsr-iet/wabesense/datalogger_hardware}{hardware} and \href{https://gitlab.com/hsr-iet/wabesense/datalogger_firmware}{firmware} repositories.
The data-logger was developed using FOSS software whenever possible, in particular we used \href{https://kicad.org/}{KiCad} for the PCB.
Fig.~\ref{fig:logger} shows a render of the PCB of the logger.

\begin{figure}
  \includegraphics[width=0.5\textwidth]{datalogger_zeroSeries_PCB_Top_annotated.png}~%
  \includegraphics[width=0.5\textwidth]{datalogger_zeroSeries_PCB_Bottom_annotated.png}
  \caption{\label{fig:logger} Zero-series data-logger. PCB renderings of (left) top and (right) bottom sides. The firmware flashing connector can be seen just above the WABEsense logo on the bottom side.}
\end{figure}

Below are the main features of the zero-series version of the data-logger.

\begin{itemize}
\item External channels
  \begin{itemize}
    \item 4x Analog 0-5 V
    \item 2x Analog 0-3.3 V (VDDA)
    \item 7x Digital In/Out
    \item SPI interface or additional 4x Digital In/Out
    \item I2C interface or additional 2x Digital In/Out
    \item V Supply (USB or external DC-Adapter)
    \item 3.3 V supply
    \item 3.3 V and 17 V On/Off-controlled by the micro-controller
    \item 3.3 V VDDA supply 
  \end{itemize}
\item Internal channels
  \begin{itemize}
    \item \href{https://www.bosch-sensortec.com/products/environmental-sensors/humidity-sensors-bme280/}{BME280} sensor: pressure, temperature, humidity
  \end{itemize}
\item Data recording
  \begin{itemize}
    \item \href{https://www.adestotech.com/wp-content/uploads/AT25SF321B.pdf}{Internal FLASH} (4 MB): up to 130000 data points (> 2 years @10 minute sampling interval)
  \end{itemize}
\item Data output exported to external memory stick (USB-C connector)
  \begin{itemize}
    \item Channel values (CSV file) 
    \item Metadata and configuration (Plain text file)
    \item Logger activity log (Plain text file)
  \end{itemize}
\item Input power source
  \begin{itemize}
    \item USB-C connector: 5 V DC
    \item External DC-Adapter (plug 2.1 x 5.5 mm): 5-28 V DC or 
  \end{itemize}
\item Configuration over USB-C connector (virtual COM-Port):
  \begin{itemize}
    \item Device name string: max. 92 characters
    \item Analog channel name string: max. 64 characters
    \item Battery: nominal cell voltage, number of cells (used for system check)
    \item Sampling interval: 1-1092 minutes (18.2 hours)
    \item System time: RTC with back-up battery CR2032 (RTC unit is integrated in micro-controller)
  \end{itemize}
\item Firmware update:
  \begin{itemize}
      \item Via memory stick using .bin (binary, compiled firmware) files (USB-C connector)
  \end{itemize}
\end{itemize}

\subsection{Changes from alpha-series}

\paragraph{Serial interface, data transfer, and upgrade over USB-C port:} the alpha-series logger allowed the user to configure the logger and run some simple commands using a serial port over USB-B.
The SD-card was used to download the logged data and to upload new firmware versions (starting from firmware 1.6.0).
On the zero-series logger this is now all done over the USB-C port interface.
This allows for configuration, maintenance, and use of the logger all over a single interface.
The flashing of a new firmware is still done over an specialized connector on the logger.

The main reason to move to the USB interface, however, was somehow unexpected.
In the several months that we have been observing the alpha-series logger, we noted that the humidity inside the logger's housing (the logger electronics are placed inside a housing) was high for prolonged periods of time. 
That is, our measures to avoid humidity exposure (simply sealed housing with silica gel bags) were not working satisfactorily.
The need for a tighter logger housing was clear.
We did not find good housings with tight SD-card slots (e.g. IP64-66), and we did easily found them for USB-C ports.
Hence we switched the interface to USB-C, which brought the advantage mentioned above, but also forced us to update the micro-controller.
This is because the STM32L412 family, used for the alpha-series, does not support USB-C.
This also meant an update in the housing as shown in Fig.~\ref{fig:housing}.
In the new housing the SD-card slot is not accessible from the outside.

\begin{figure}[h]
  \includegraphics[width=0.5\textwidth]{datalogger_housing_test_annotated.png}~%
  \includegraphics[width=0.5\textwidth]{datalogger_housing_zero_annotated.png}
  \caption{\label{fig:housing} (left) previous and (right) new logger housing.
  In the new housing the SD-card slot is not accessible from the outside.}
\end{figure}

\paragraph{Integrated battery support:} the alpha-series logger already provides an external source input via the DC plug.
The integrated batteries increased the complexity of the logger, hence we decided to directly use the DC plug to connect the external batteries. 
This brought the additional advantage of simplifying the electronics of the logger.

\paragraph{Adding more sensor connectors:} the alpha-series logger allowed to connect 2 analog sensors.
This kept the first version simple.
Now that the functionality is tested and works well, we opened up the connections to the additional
2 analog channels.

\paragraph{Non-soldering connectors for buttons and LED:} to ease the exchange of a failing button or LEDs, these parts are integrated in the logger via connectors, no need for any (de)soldering.
 
\paragraph{ST connector for the programmer/debugger device:} to flash and debug the logger one needs a
programmer device (currently we use ST \href{https://www.st.com/en/development-tools/st-link-v2.html}{tools} and \href{https://www.st.com/en/development-tools/stm32cubeprog.html}{software}, we are in the process of liberating the toolchain from these dependencies). 
The alpha-series had a smaller connector which required an adapter to connect the ST programmer.
To simplify this, we put an ST compatible connector on the logger.

\subsection{Features kept from alpha-series data-logger}

\paragraph{Internal flash memory:} the conceptual design of the logger did not have any internal
storage.
The final design of the alpha-series was upgraded to include a 4 MB FLASH memory module.
This allows for simple management of the logged data, and for low sampling rate applications the available storage is plenty.

\paragraph{Working SD-card module:} although the data transfer on the zero-series happens over the USB-C port a functional SD-card module was kept in the logger. 
The SD-card is like a hard-disk.
This is because having the possibility to add large non-volatile storage has many advantages.
For example, if the logger is set to log every minute (the current minimum sampling period) the internal storage would last for little more than 90 days. 
In such scenario data could be dumped into an SD-card and extend the logging records to many years.
Another scenario is larger amounts of metadata.
Currently, information about the logger (logs, configuration, etc.) is stored in the internal FLASH, this takes up space and could be stored in an SD-card. 
This also allows to store more metadata like sensor calibrations, maintenance notes, location information, etc.

\section{CFD Q-Sense: CFD based discharge curves}

The \emph{CFD Q-sense} python module automatizes the generation of OpenFOAM case folders to produce pressure-discharge curve for water spring taps.
The module is publicly available under the \href{https://www.gnu.org/licenses/gpl-3.0.en.html}{GNU General Public License}, and can be downloaded from \href{https://gitlab.com/hsr-iet/wabesense/cfd_qsense}{its repository}.
The module \href{https://hsr-iet.gitlab.io/wabesense/cfd_qsense/}{documentation} is also available online.
The documentation provides several examples of use, including full tutorials like the one previewed in Fig.~\ref{fig:discharge-curve}.

\begin{figure}[h]
  \includegraphics[width=0.5\textwidth]{qsense_lateral_weir_annotated.png}
  \includegraphics[width=0.5\textwidth]{qsense_lateral_weir_discharge.png}
  \caption{\label{fig:discharge-curve}. CFD Q-sense discharge curve. The left panel shows an hypothetical spring box with inlet (green), outlet (red), and open to atmospheric conditions (blue). The right panel shows the discharge curves generated with OpenFOAM for four pressure sensor locations.
The \href{https://hsr-iet.gitlab.io/wabesense/cfd_qsense/auto_examples/plot_DischargeCase.html}{full example} is found in the documentation of the module.}
\end{figure}

This module implements functions to automatically generate OpenFOAM parametrized case files\footnote{Parametrized OpenFOAM files are explained in this blog post \url{http://sciceg.pages.gitlab.ost.ch/blogs/juanpi/post/openfoam_parametric/}}.
The output of the module’s functions are text files that configure an OpenFOAM simulation.
The module does not run the simulations.
To do that the user needs OpenFOAM installed in their system.

The simulation procedure was validated in the laboratory for the case of WABE containers in a previous project funded by ULAG during the year 2020.
Manual discharge measurements taken at the well are used to verify the quality of the obtained discharge curve periodically. 
Fig.~\ref{fig:dc_verification} shows a comparison between the computed discharge (3h moving average, in blue) and the manual measurements (red).

\begin{figure}
  \includegraphics[width=0.95\textwidth]{Paliu_Fravi_Q_3havg_final.png}
  \caption{\label{fig:dc_verification} Computed and measured discharge. 
  Comparison between the computed discharge (3h moving average, in blue) and the manual measurements (red) at the Paliu Fravi well.}
\end{figure}

The diagram in Fig.~\ref{fig:qsense} summarizes the information flow for a typical OpenFOAM simulation.
The case folder\footnote{See file structure of OpenFOAM cases \url{https://cfd.direct/openfoam/user-guide/v8-case-file-structure/}} configures the simulation that will be run.
This module is concerned mainly with the managing of that folder, but it also provides some support for the post-processing of OpenFOAM output files.

The module also provides some convenience classes for physical properties of water and humid air, based on the \href{http://www.coolprop.org/}{CoolProp library (python wrapper)}.

\begin{figure}[h]
  \includegraphics[width=\textwidth]{qsense_module_concern.png}
  \caption{\label{fig:qsense} CFD Q-sense module's concern. 
  The module manages the case folder that is consumed by OpenFOAM and support basic post-processing of OpenFOAM outputs.
  The STL files defining the domain of simulation are provided by the user.}
\end{figure}

There are two workflows that this module supports: i) create user defined case files, ii) parametrized existing case files.
The first workflow is general and the module provides basic support for it\footnote{See an example here \url{https://hsr-iet.gitlab.io/wabesense/cfd_qsense/auto_examples/plot_controlDict.html}}.
It is the aim to offer a better support for this general workflow in future versions.
The second workflow is more specific.
It essentially boils down to copying an already existing OpenFOAM case folder and then editing the files to add parameters (character strings prefixed with the "at" symbol @) that this module will replace with values when executed\footnote{The parametrized OpenFOAM pitzDaily tutorial is shown here \url{https://hsr-iet.gitlab.io/wabesense/cfd_qsense/auto_examples/plot_pitzDaily.html}}.

The module allows the user to write python scripts that automatizes parametric studies, which typically include: mesh convergences studies, domain geometry exploration, sampling for uncertainty quantification, etc.

In the WABEsense project we use this module to generate the discharge curves of all the WABE models that we have found in the field and the new designs that are being installed.
We also use it to study discharge curves of other spring boxes (not WABE) to
test the applicability of the WABEsense solution to other water tappings.


\end{document}
